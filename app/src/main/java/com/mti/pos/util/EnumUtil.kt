package com.mti.pos.util

object EnumUtil {
    enum class SharedPref(val key: String) {
        TransCode("trans_code") // Integer
    }

    enum class UserType(val code: Int) {
        Salesman(0),
        Supervisor(1)
    }

    enum class TransType(val code: Int) {
        Cash(0),
        Credit(1)
    }

    enum class MutasiStatus(val code: Int) {
        Pending(0),
        Approve(1),
        Reject(2)
    }

}



