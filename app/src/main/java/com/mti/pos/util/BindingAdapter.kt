package com.mti.pos.util

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.adapters.TextViewBindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    view.visibility = if (isGone) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("isError")
fun editTextError(textInputLayout: TextInputLayout, isError: Boolean) {
    textInputLayout.isErrorEnabled = isError
    textInputLayout.error = if (isError) {
        "Data tidak valid"
    } else {
        ""
    }
}

@BindingAdapter("android:onTextChanged")
fun setListener(view: TextView?, after: TextViewBindingAdapter.OnTextChanged?) {
    setListener(view, after)
}