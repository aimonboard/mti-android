package com.mti.pos.util

import android.util.Log
import com.dantsu.escposprinter.EscPosPrinter
import com.dantsu.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.mti.pos.data.database.entity.TransactionDetailEntity
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.data.model.ProductStockModel
import com.mti.pos.data.model.ReportPrintModel
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class BluetoothUtil {

    fun enableBluetooth(turnOn: Boolean) {
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        if (turnOn) {
//            mBluetoothAdapter.enable()
//        } else {
//            mBluetoothAdapter.enable()
//        }
    }

    fun printTransaction (
        transactionHeader: TransactionEntity,
        transactionDetail: List<TransactionDetailEntity>
    ) {
        val format = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault())
        val now = format.format(Date())

        val localeID = Locale("in", "ID")
        val rupiah = NumberFormat.getCurrencyInstance(localeID)

        var printString = ""
        printString += "[C]Mustika Tembakau\n"
        printString += "[C]Indonesia\n"

        printString += "--------------------------------\n"
        printString += "[L]No: ${transactionHeader.transCode}\n"
        printString += "[L]Tanggal: $now\n"
        printString += "[L]Customer: ${transactionHeader.trans_customerName}\n"
        printString += "[L]Salesman: ${transactionHeader.trans_salesName}\n"
        printString += "--------------------------------\n"

        var total = 0.0
        transactionDetail.forEach {
            val productName = it.transDetail_productName
            val price = it.transDetailPrice
            val qty = it.transDetailQtyPCS

            val priceQtyString = "${rupiah.format(price)} (${qty})"
            val subTotal = price * qty
            total += subTotal

            Log.e("aim", "$price * $qty = $subTotal")

            printString += "[L]${productName}\n"
            printString += "[L]${priceQtyString}[R]${rupiah.format(subTotal)}\n"
        }

        printString += " \n\n"

        printString += "[L]Total[R]${rupiah.format(total)}\n"
        printString += "[L]Pembayaran[R]${rupiah.format(transactionHeader.transPayment)}\n"
        printString += "[L]Kembalian[R]${rupiah.format(transactionHeader.transCashBack)}\n"

        printString += " \n\n"

        Log.e("aim", "\n============== PRINT ==============\n$printString")

        val printer = EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(), 203, 48f, 32)
        printer.printFormattedText(printString)
    }

    fun printDailyReport (data: ReportPrintModel, stok: List<ProductStockModel>) {
        val format = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault())
        val now = format.format(Date())

        var printString = ""
        printString += "[C]Mustika Tembakau\n"
        printString += "[C]Indonesia\n"
        printString += "--------------------------------\n"
        printString += "[L]${data.name}\n"
        printString += "[L]$now\n"
        printString += "--------------------------------\n"

        printString += " \n\n"

//        printString += "[L]Stok Barang[R]${data.stock}\n"

        printString += "[L]Stok Barang :\n"
        stok.forEachIndexed { index, it ->
            var productName = it.productName.replace("Rokok ","")
            productName = productName.replace("ROKOK ","")
            printString += "[L]${index+1}. $productName (${it.stockProductQty})\n"
        }

        printString += " \n\n"

        printString += "[L]Barang Keluar[R]${data.productOut}\n"

        val totalMoney = data.total.split(" - ")
        printString += "[L]Total Transaksi[R]${totalMoney[1]}\n"

        printString += " \n\n"

        printString += "[C]Salesman\n"

        printString += " \n\n\n\n\n\n"

        printString += "[C](${data.name})\n"

        printString += " \n\n"

        Log.e("aim", "\n============== PRINT ==============\n$printString")

        val printer = EscPosPrinter(BluetoothPrintersConnections.selectFirstPaired(), 203, 48f, 32)
        printer.printFormattedText(printString)
    }

}