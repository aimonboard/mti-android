package com.mti.pos.util

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.mti.pos.ui.salesman.MenuSalesmanActivity
import java.util.*

class GpsUtil(private val context: Context) {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    fun enableGps() {
        val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager?
        if (locationManager != null) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showAlertLocation()
            }
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
            getLocationUpdates()
        }
    }

    private fun showAlertLocation() {
        val dialog = AlertDialog.Builder(context)
        dialog.setCancelable(false)
        dialog.setMessage("Aktifkan fitur lokasi / GPS")
        dialog.setPositiveButton("Pengaturan") { _, _ ->
            val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            context.startActivity(myIntent)
        }
        dialog.show()
    }

    private fun getLocationUpdates() {
        locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            fastestInterval = 1000 * 60
            interval = 1000 * 120
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                if (locationResult.locations.isNotEmpty()) {
                    val location = locationResult.locations.first()
                    MenuSalesmanActivity.lat = location.latitude.toString()
                    MenuSalesmanActivity.lon = location.longitude.toString()
                }
            }
        }
    }

    fun startLocationUpdates() {
        val result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)

        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        }
    }

    fun stopLocationUpdates() {
        if (this::fusedLocationClient.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    fun getAddressFromLatLong(context: Context, latitude: Double, longitude: Double): String {
        val geoCoder = Geocoder(context, Locale.getDefault())
        val addressList = geoCoder.getFromLocation(latitude, longitude, 1)

        return if (!addressList.isNullOrEmpty()) {
            addressList[0].getAddressLine(0)
        } else {
            ""
        }
    }

    fun sortLocations(): List<DummyLocation> {
        val dummyLocation = ArrayList<DummyLocation>()
        dummyLocation.add(
            DummyLocation(
                name = "Demak",
                location = createLocationObject(-7.240607831409197, 112.72065447488944)
            )
        )
//        dummyLocation.add(
//            DummyLocation(
//                name = "Tatik",
//                location = createLocationObject(-7.255851352315628, 112.71998877034534)
//            )
//        )
        dummyLocation.add(
            DummyLocation(
                name = "Pahlawan",
                location = createLocationObject(-7.246101706553947, 112.7385488050919)
            )
        )

        val myLatitude = MenuSalesmanActivity.lat.toDouble()
        val myLongitude = MenuSalesmanActivity.lon.toDouble()

        val comp = Comparator<DummyLocation> { o, o2 ->
            val result1 = FloatArray(3)
            Location.distanceBetween(
                myLatitude,
                myLongitude,
                o.location.latitude,
                o.location.longitude,
                result1
            )
            val distance1 = result1[0]
            val result2 = FloatArray(3)
            Location.distanceBetween(
                myLatitude,
                myLongitude,
                o2.location.latitude,
                o2.location.longitude,
                result2
            )
            val distance2 = result2[0]
            distance1.compareTo(distance2)
        }
        Collections.sort(dummyLocation, comp)

        return dummyLocation
    }

    fun generateRouteUrl(dummyLocation: List<DummyLocation>): String {
        var url = "https://www.google.com/maps/dir/?api=1" +
                "&origin=${MenuSalesmanActivity.lat},${MenuSalesmanActivity.lon}"
        when {
            dummyLocation.size == 1 -> {
                url += "&destination=${dummyLocation.first().location.latitude},${dummyLocation.first().location.longitude}"
            }
            dummyLocation.size >= 2 -> {
                url += "&destination=${dummyLocation.last().location.latitude},${dummyLocation.last().location.longitude}&waypoints="

                dummyLocation.forEachIndexed { index, location ->
                    if (index != dummyLocation.lastIndex) {
                        url += "${location.location.latitude},${location.location.longitude}|"
                    }
                }
            }
        }
        url += "&travelmode=driving"

        return url
    }

    private fun createLocationObject(lat: Double, lon: Double): Location {
        return Location(LocationManager.GPS_PROVIDER).apply {
            latitude = lat
            longitude = lon
        }
    }

    data class DummyLocation(
        val name: String,
        val location: Location
    )

}