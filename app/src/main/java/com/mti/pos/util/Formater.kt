package com.mti.pos.util

import java.text.NumberFormat
import java.util.*

class Formater {
    fun idrFormat(priceValue: Double): String {
        val localeID = Locale("in", "ID")
        val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
        return formatRupiah.format(priceValue)
            .replace("Rp", "")
            .replace(",00","")
    }
}