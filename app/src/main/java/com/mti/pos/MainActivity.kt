package com.mti.pos

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.LoginRequest
import com.mti.pos.data.retrofit.ApiService
import com.mti.pos.ui.salesman.MenuSalesmanActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var remoteDataSource: ApiService

    @Inject
    lateinit var localDataSource: AppDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonListener()
        checkSavedLogin()
    }

    private fun buttonListener() {
        btn_login.setOnClickListener {
            if (checkInput()) {
                login()
            }
        }

        btn_forgot.setOnClickListener {

        }
    }

    private fun checkSavedLogin() {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        profileData.observe(this@MainActivity, Observer {
            if (it.isNotEmpty()) {
                val i = Intent(this@MainActivity, MenuSalesmanActivity::class.java)
                startActivity(i)
                finish()

//                val i = Intent(this@MainActivity, MenuSupervisorActivity::class.java)
//                startActivity(i)
//                finish()
            }
        })
    }

    private fun checkInput(): Boolean {
        var cek = 0

        if (user_value.text.toString() == "") {
            user_lay.error = "Kolom tidak boleh kosong"; cek++
        } else {
            user_lay.isErrorEnabled = false
        }

        if (pass_value.text.toString() == "") {
            pass_lay.error = "Kolom tidak boleh kosong"; cek++
        } else {
            pass_lay.isErrorEnabled = false
        }

        if (cek == 0) {
            return true
        }
        return false
    }

    private fun login() {
        progress_bar.visibility = View.VISIBLE
        btn_login.isEnabled = false

        val params = HashMap<String, String>()
        params["username"] = user_value.text.toString()
        params["password"] = pass_value.text.toString()
        Log.e("aim", "param : $params")

        remoteDataSource.login(params).enqueue(object : Callback<LoginRequest> {
            override fun onFailure(call: Call<LoginRequest>, t: Throwable) {
                progress_bar.visibility = View.GONE
                btn_login.isEnabled = true
            }

            override fun onResponse(call: Call<LoginRequest>, response: Response<LoginRequest>) {
                progress_bar.visibility = View.GONE
                btn_login.isEnabled = true

                if (response.isSuccessful) {
                    Log.e("aim", response.body().toString())
                    val statusCode = response.body()?.statusCode ?: 0
                    if (statusCode == 200) {
                        GlobalScope.launch {
                            localDataSource.profileInsert(
                                ProfileEntity(
                                    userId = response.body()?.data?.staffId ?: 0,
                                    userName = response.body()?.data?.staffName ?: "",
                                    warehouseCode = response.body()?.data?.warehouseCode ?: "",
                                    userType = response.body()?.data?.staffStatus ?: 0,
                                    transType = response.body()?.data?.transType ?: 0,
                                    lastSync = ""
                                )
                            )
                        }

                        val i = Intent(this@MainActivity, MenuSalesmanActivity::class.java)
                        startActivity(i)
                        finish()
                    } else {
                        val errMessage = response.body()?.errorMessage ?: ""
                        Toast.makeText(this@MainActivity, errMessage, Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

}