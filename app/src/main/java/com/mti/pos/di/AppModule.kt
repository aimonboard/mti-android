package com.mti.pos.di

import android.bluetooth.BluetoothAdapter
import android.content.Context
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.AppDatabase
import com.mti.pos.data.retrofit.ApiClient
import com.mti.pos.data.retrofit.ApiService
import com.mti.pos.ui.salesman.customer.CustomerRepository
import com.mti.pos.ui.salesman.customer_add.CustomerAddRepository
import com.mti.pos.ui.salesman.cuti.CutiRepository
import com.mti.pos.ui.salesman.cuti_form.CutiFormRepository
import com.mti.pos.ui.salesman.home.HomeRepository
import com.mti.pos.ui.salesman.mutasi.MutasiRepository
import com.mti.pos.ui.salesman.mutasi_add.MutasiAddRepository
import com.mti.pos.ui.salesman.mutasi_detail.MutasiDetailRepository
import com.mti.pos.ui.salesman.product.ProductRepository
import com.mti.pos.ui.salesman.profile.ProfileRepository
import com.mti.pos.ui.salesman.report.ReportRepository
import com.mti.pos.ui.salesman.report_detail.ReportDetailRepository
import com.mti.pos.ui.salesman.setting.SettingRepository
import com.mti.pos.ui.salesman.transaction.TransactionRepository
import com.mti.pos.ui.salesman.transaction_detail.TransactionDetailRepository
import com.mti.pos.util.BluetoothUtil
import com.mti.pos.util.GpsUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    // ============================================================================================= Retrofit
    @Singleton
    @Provides
    fun provideApiClient(): Retrofit = ApiClient().buildRetrofit()

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)


    // ============================================================================================= Database
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Provides
    fun provideDatabaseDao(db: AppDatabase) = db.dao()

    // ============================================================================================= Bluetooth
    @Singleton
    @Provides
    fun provideBluetooth() = BluetoothUtil()

    @Singleton
    @Provides
    fun provideBluetoothAdapter(): BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    // ============================================================================================= GPS
    @Singleton
    @Provides
    fun provideGps(@ApplicationContext appContext: Context) = GpsUtil(appContext)

    // ============================================================================================= Repository
    @Singleton
    @Provides
    fun homeRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao,
        btAdapter: BluetoothAdapter,
        btUtil: BluetoothUtil
    ) = HomeRepository(
        remoteDataSource,
        localDataSource,
        btAdapter,
        btUtil
    )

    @Singleton
    @Provides
    fun profileRepository(localDataSource: AppDao, database: AppDatabase) =
        ProfileRepository(localDataSource, database)

    @Singleton
    @Provides
    fun cutiRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) = CutiRepository(
        remoteDataSource,
        localDataSource
    )

    @Singleton
    @Provides
    fun cutiFormRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) = CutiFormRepository(
        remoteDataSource,
        localDataSource
    )

    @Singleton
    @Provides
    fun customerRepository(localDataSource: AppDao) = CustomerRepository(localDataSource)

    @Singleton
    @Provides
    fun customerAddRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) = CustomerAddRepository(
        remoteDataSource,
        localDataSource
    )

    @Singleton
    @Provides
    fun productRepository(localDataSource: AppDao) = ProductRepository(localDataSource)

    @Singleton
    @Provides
    fun mutasiRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) = MutasiRepository(
        remoteDataSource,
        localDataSource
    )

    @Singleton
    @Provides
    fun mutasiAddRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) =
        MutasiAddRepository(
            remoteDataSource,
            localDataSource
        )

    @Singleton
    @Provides
    fun mutasiDetailRepository(
        remoteDataSource: ApiService,
        localDataSource: AppDao
    ) = MutasiDetailRepository(
        remoteDataSource,
        localDataSource
    )

    @Singleton
    @Provides
    fun transactionRepository(remoteDataSource: ApiService, localDataSource: AppDao) =
        TransactionRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun transactionDetailRepository(
        localDataSource: AppDao,
        btAdapter: BluetoothAdapter,
        btUtil: BluetoothUtil
    ) = TransactionDetailRepository(
        localDataSource,
        btAdapter,
        btUtil
    )

    @Singleton
    @Provides
    fun reportRepository(localDataSource: AppDao) = ReportRepository(localDataSource)

    @Singleton
    @Provides
    fun reportDetailRepository(
        localDataSource: AppDao,
        btAdapter: BluetoothAdapter,
        btUtil: BluetoothUtil
    ) = ReportDetailRepository(
        localDataSource,
        btAdapter,
        btUtil
    )

    @Singleton
    @Provides
    fun settingRepository(btAdapter: BluetoothAdapter) = SettingRepository(btAdapter)


}