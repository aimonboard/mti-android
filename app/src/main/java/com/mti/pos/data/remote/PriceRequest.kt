package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class PriceRequest(
    @SerializedName("statusCode")
    val statusCode: Int = 0,
    @SerializedName("data")
    val `data`: List<Data> = listOf()
) {
    data class Data(
        @SerializedName("group_id")
        val groupId: Int = 0,
        @SerializedName("group_name")
        val groupName: String = "",
        @SerializedName("group_code")
        val groupCode: String = "",
        @SerializedName("price_list")
        val priceList: List<Price> = listOf()
    ) {
        data class Price(
            @SerializedName("id")
            val priceId: Int = 0,
            @SerializedName("item_id")
            val itemId: Int = 0,
            @SerializedName("measurement")
            val measurement: String = "",
            @SerializedName("price")
            val price: Double = 0.0,
            @SerializedName("start_date")
            val startDate: String = "",
            @SerializedName("end_date")
            val endDate: String = ""
        )
    }
}