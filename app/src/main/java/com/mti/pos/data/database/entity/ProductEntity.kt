package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product")
data class ProductEntity (
    @PrimaryKey(autoGenerate = false)
    val productId: Int,
    val productCode: String,
    val productName: String
)