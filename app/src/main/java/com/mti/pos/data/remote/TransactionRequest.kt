package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class TransactionRequest(
    @SerializedName("statusCode")
    val statusCode: Int? = 0,
    @SerializedName("invoice_id")
    val invoiceId: String? = ""
)