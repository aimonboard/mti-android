package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profile")
data class ProfileEntity(
    @PrimaryKey(autoGenerate = false)
    val userId: Int,
    val userName: String,
    val warehouseCode: String,
    val userType: Int,
    val transType: Int,
    val lastSync: String
)