package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class MutasiListRequest(
    @SerializedName("data")
    val `data`: List<Data?>?,
    @SerializedName("statusCode")
    val statusCode: Int?
) {
    data class Data(
        @SerializedName("approve_by")
        val approveBy: List<ApproveBy?>?,
        @SerializedName("approved_by")
        val approvedBy: Any?,
        @SerializedName("created_by")
        val createdBy: String?,
        @SerializedName("date")
        val date: String?,
        @SerializedName("mutasi_id")
        val mutasiId: String?,
        @SerializedName("nomor_mutasi")
        val nomorMutasi: String?,
        @SerializedName("product")
        val product: List<Product?>?,
        @SerializedName("request_by")
        val requestBy: List<RequestBy?>?,
        @SerializedName("status")
        val status: String?,
        @SerializedName("warehouse_code")
        val warehouseCode: String?
    ) {
        data class ApproveBy(
            @SerializedName("salesman_id")
            val salesmanId: String?,
            @SerializedName("staff_name")
            val staffName: String?
        )

        data class Product(
            @SerializedName("item_id")
            val itemId: String?,
            @SerializedName("item_name")
            val itemName: String?,
            @SerializedName("qty")
            val qty: String?
        )

        data class RequestBy(
            @SerializedName("salesman_id")
            val salesmanId: String?,
            @SerializedName("staff_name")
            val staffName: String?
        )
    }
}