package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class ProductRequest(
    @SerializedName("statusCode")
    val statusCode: Int = 0,
    @SerializedName("data")
    val `data`: List<Data> = listOf()
) {
    data class Data(
        @SerializedName("item_id")
        val itemId: Int = 0,
        @SerializedName("item_name")
        val itemName: String = "",
        @SerializedName("item_code")
        val itemCode: String = "",
        @SerializedName("stock")
        val stock: Int = 0,
        @SerializedName("detail")
        val detail: List<Detail> = listOf()
    ) {
        data class Detail(
            @SerializedName("measurement")
            val measurement: String = "",
            @SerializedName("conversion")
            val conversion: String = ""
        )
    }
}