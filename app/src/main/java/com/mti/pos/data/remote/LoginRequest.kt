package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("statusCode")
    val statusCode: Int = 0,
    @SerializedName("error")
    val errorMessage: String? = "",
    @SerializedName("data")
    val `data`: Data = Data()
) {
    data class Data(
        @SerializedName("staff_id")
        val staffId: Int = 0,
        @SerializedName("staff_name")
        val staffName: String = "",
        @SerializedName("warehouse_code")
        val warehouseCode: String = "",
        @SerializedName("staff_status")
        val staffStatus: Int = 0,
        @SerializedName("salesman_type")
        val transType: Int = 0,
    )
}