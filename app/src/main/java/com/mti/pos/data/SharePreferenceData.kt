package com.mti.pos.data

import android.content.Context

class SharePreferenceData {

    companion object {
        private const val PREF_FILE = "mti_data"

        fun setInt(context: Context?, key: String, value: Int) {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            val editor = settings?.edit()
            editor?.putInt(key, value)
            editor?.apply()
        }

        fun getInt(context: Context?, key: String, defValue: Int): Int {
            val settings = context?.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
            return settings?.getInt(key, defValue) ?: defValue
        }
    }
}