package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "price")
data class PriceListEntity (
    @PrimaryKey(autoGenerate = false)
    val priceListId: Int,
    val priceList_customerTypeId: Int,
    val priceList_productId: Int,
    val priceListMeasurement: String,
    val priceListPrice: Double,
    val priceListStartDate: String,
    val priceListEndDate: String
)