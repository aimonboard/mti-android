package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class CutiRequest(
    @SerializedName("statusCode")
    val statusCode: Int? = 0,
    @SerializedName("data")
    val `data`: List<Data?>? = listOf()
) {
    data class Data(
        @SerializedName("leave_type_name")
        val leaveTypeName: String? = "",
        @SerializedName("leave_type_id")
        val leaveTypeId: String? = "",
        @SerializedName("start_date")
        val startDate: String? = "",
        @SerializedName("end_date")
        val endDate: String? = "",
        @SerializedName("request_date")
        val requestDate: String? = "",
        @SerializedName("status")
        val status: String? = "",
        @SerializedName("status_name")
        val statusName: String? = "",
        @SerializedName("notes")
        val notes: String? = ""
    )
}