package com.mti.pos.data.model

data class ProductOut(
    val productOutQty: Int,
    val productOutMoney: Double
)
