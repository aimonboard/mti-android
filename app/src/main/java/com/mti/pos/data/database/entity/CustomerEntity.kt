package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customer")
data class CustomerEntity(
    @PrimaryKey(autoGenerate = false)
    val customerId: Int,
    val customerCode: String,
    val customerName: String,
    val customerAddress: String,
    val customer_customerTypeId: Int,
    val customerPhone: String,
    val customerLat: String,
    val customerLon: String,
    val customerIsRegistered: Int
)