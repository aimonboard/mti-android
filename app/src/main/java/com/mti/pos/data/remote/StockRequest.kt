package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class StockRequest(
    @SerializedName("statusCode")
    val statusCode: Int? = 0,
    @SerializedName("data")
    val `data`: List<Data?>? = listOf()
) {
    data class Data(
        @SerializedName("iditem")
        val iditem: String? = "",
        @SerializedName("stock_in")
        val stockIn: String? = ""
    )
}