package com.mti.pos.data.model

data class TransactionModel(
    val type: Int,
    val code: String,
    val date: String,
    val customerId: Int,
    val customerName: String,
    val salesId: Int,
    val salesName: String,
    val total: Double,
    val discount: Double,
    val grandTotal: Double,
    val payment: Double,
    val cashBack: Double,
    val latitude: String,
    val longitude: String,
    val productData: ArrayList<ProductStockPriceModel>
)
