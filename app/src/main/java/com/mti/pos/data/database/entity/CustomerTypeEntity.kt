package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customer_type")
data class CustomerTypeEntity (
    @PrimaryKey(autoGenerate = false)
    val customerTypeId: Int,
    val customerTypeCode: String,
    val customerTypeName: String,
)