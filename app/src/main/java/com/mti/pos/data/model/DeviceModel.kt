package com.mti.pos.data.model

data class DeviceModel (
    val deviceName: String,
    val deviceAddress: String
)