package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction_detail")
data class TransactionDetailEntity (
    @PrimaryKey(autoGenerate = true)
    val transDetailId: Int,
    val transDetail_transId: Int,
    val transDetail_productId: Int,
    val transDetail_productName: String,
    val transDetailMeasurement: String,
    val transDetailQtyUOM: Int,
    val transDetailConversion: Int,
    val transDetailQtyPCS: Int,
    val transDetailPrice: Double,
    val transDetailDiscount: Double,
    val transDetailSubTotal: Double,
    val transDetailDate: String,
)