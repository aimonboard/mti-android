package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class CustomerRequest(
    @SerializedName("statusCode")
    val statusCode: Int,
    @SerializedName("data")
    val `data`: List<Data> = listOf()
) {
    data class Data(
        @SerializedName("customer_id")
        val customerId: Int?,
        @SerializedName("customer_code")
        val customerCode: String?,
        @SerializedName("customer_name")
        val customerName: String?,
        @SerializedName("customer_address")
        val customerAddress: String?,
        @SerializedName("customer_group_id")
        val customerGroupId: Int?,
        @SerializedName("customer_phone")
        val customerPhone: String?,
        @SerializedName("customer_lat")
        val customerLat: String?,
        @SerializedName("customer_lon")
        val customerLon: String?,
        @SerializedName("customer_isRegistered")
        val customerIsRegistered: Int?
    )
}