package com.mti.pos.data.retrofit

import com.mti.pos.data.remote.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ApiService {

    @Headers("Accept: application/json")
    @POST("login")
    fun login(
        @Body body: HashMap<String, String>
    ): Call<LoginRequest>

    @Headers("Accept: application/json")
    @POST("listcustomer")
    fun customer(
        @Body body: HashMap<String, String>
    ): Call<CustomerRequest>

    @Headers("Accept: application/json")
    @POST("add-customer")
    fun customerAdd(
        @Body body: HashMap<String, String>
    ): Call<CustomerRequest>

    @Headers("Accept: application/json")
    @POST("edit-customer")
    fun customerEdit(
        @Body body: HashMap<String, String>
    ): Call<CustomerRequest>

    @Headers("Accept: application/json")
    @POST("listitem")
    fun product(
        @Body body: HashMap<String, String>
    ): Call<ProductRequest>

    @Headers("Accept: application/json")
    @POST("stockbalance")
    fun stock(
        @Body body: HashMap<String, String>
    ): Call<StockRequest>

    @Headers("Accept: application/json")
    @POST("listcustomergroup")
    fun price(): Call<PriceRequest>

    @Headers("Accept: application/json")
    @POST("checkout-invoice")
    fun transaction(
        @Body body: RequestBody
    ): Call<TransactionRequest>

    @Headers("Accept: application/json")
    @POST("listcuti")
    fun getCuti(
        @Body body: HashMap<String, String>
    ): Call<CutiRequest>

    @Headers("Accept: application/json")
    @POST("add-cuti")
    fun sendCuti(
        @Body body: HashMap<String, String>
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("list-mutation-request")
    fun getMutasi(
        @Body body: HashMap<String, String>
    ): Call<MutasiListRequest>

    @Headers("Accept: application/json")
    @POST("add-mutation-request")
    fun sendMutasi(
        @Body body: RequestBody
    ): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("approve-mutation-request")
    fun setMutasi(
        @Body body: HashMap<String, String>
    ): Call<ResponseBody>

    @GET
    fun getAllCharacters(
        @Url url: String,
    ): Call<TestModel>

}