package com.mti.pos.data.model

data class ProductStockPriceModel (
    var productId: Int,
    var productCode: String,
    var productName: String,

    var stockProductId: Int?,
    var stockProductQty: Int?,

    var priceListId: Int?,
    var priceList_customerTypeId: Int?,
    var priceList_productId: Int?,
    var priceListMeasurement: String?,
    var priceListPrice: Double?,
    var priceListStartDate: String?,
    var priceListEndDate: String?
)