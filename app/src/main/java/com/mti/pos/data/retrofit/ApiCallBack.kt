package com.mti.pos.data.retrofit

interface ApiCallBack {
    fun onRequestFinish(message: String)
}