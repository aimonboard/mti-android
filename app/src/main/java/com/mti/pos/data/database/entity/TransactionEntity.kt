package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction")
data class TransactionEntity(
    @PrimaryKey(autoGenerate = true)
    val transId: Int,
    val transType: Int,
    val transCode: String,
    val transDate: String,
    val trans_customerId: Int,
    val trans_customerName: String,
    val trans_salesId: Int,
    val trans_salesName: String,
    val transTotal: Double,
    val transDiscount: Double,
    val transGrandTotal: Double,
    val transPayment: Double,
    val transCashBack: Double,
    val transLatitude: String,
    val transLongitude: String,
    val transIsUpload: Boolean
)