package com.mti.pos.data.model

data class ProductQtyModel(
    val productId: Int,
    val productCode: String,
    val productName: String,

    var productQty: Int? = 0
)