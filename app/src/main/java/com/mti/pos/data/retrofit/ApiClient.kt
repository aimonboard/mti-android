package com.mti.pos.data.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    private val baseUrl =
        "http://ec2-54-179-167-141.ap-southeast-1.compute.amazonaws.com/mti/restcontroller.php/"

    fun buildRetrofit(): Retrofit {
        return Retrofit.Builder().run {
            val gson = GsonBuilder()
                .enableComplexMapKeySerialization()
                .setPrettyPrinting()
                .create()

            baseUrl(baseUrl)
            addConverterFactory(GsonConverterFactory.create(gson))
            client(createRequestInterceptorClient())
            build()
        }
    }

    private fun createRequestInterceptorClient(): OkHttpClient {
        val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

        return OkHttpClient.Builder()
            .addInterceptor(logger)
            .cache(null)
            .build()

//        val interceptor = Interceptor { chain ->
//            val original = chain.request()
//            val requestBuilder = original.newBuilder()
//            val request = requestBuilder.build()
//            chain.proceed(request)
//        }

//        return OkHttpClient.Builder()
//            .addInterceptor(interceptor)
//            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//            .connectTimeout(3000, TimeUnit.SECONDS)
//            .readTimeout(3000, TimeUnit.SECONDS)
//            .writeTimeout(3000, TimeUnit.SECONDS)
//            .cache(null)
//            .build()
    }
}