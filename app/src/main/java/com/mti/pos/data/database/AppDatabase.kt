package com.mti.pos.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mti.pos.data.database.entity.*

@Database(
    entities = [
        ProfileEntity::class,
        CustomerEntity::class,
        CustomerTypeEntity::class,
        ProductEntity::class,
        PriceListEntity::class,
        StockEntity::class,
        TransactionEntity::class,
        TransactionDetailEntity::class
    ],
    version = 13,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): AppDao

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, "MTI_POS")
                .fallbackToDestructiveMigration()
                .build()
    }

}