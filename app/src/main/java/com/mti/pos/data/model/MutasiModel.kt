package com.mti.pos.data.model

data class MutasiModel(
    var salesmanId: Int,
    var warehouseCode: String,
    val productData: ArrayList<ProductQtyModel>
)