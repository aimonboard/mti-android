package com.mti.pos.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mti.pos.data.database.entity.*
import com.mti.pos.data.model.ProductOut
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.data.model.ProductStockModel
import com.mti.pos.data.model.ProductStockPriceModel

@Dao
interface AppDao {

    // ============================================================================================= Profile
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun profileInsert(Entity: ProfileEntity)

    @Query("SELECT * from profile")
    fun profileGetAll(): List<ProfileEntity>

    @Query("UPDATE profile SET lastSync=:lastSync WHERE userId=:userId")
    fun profileUpdateLastSync(userId: Int, lastSync: String)

    // ============================================================================================= Customer
    @Query("SELECT * from customer WHERE customerName LIKE '%' || :searchText || '%' ORDER BY customerName ASC")
    fun customerGetAll(searchText: String): LiveData<List<CustomerEntity>>

    @Query("SELECT * from customer WHERE customerId = :customerId")
    fun customerGetById(customerId: Int): LiveData<List<CustomerEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun customerInsert(Entity: CustomerEntity)


    // ============================================================================================= Customer Type
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun customerTypeInsert(Entity: CustomerTypeEntity)

    // ============================================================================================= Product
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun productInsert(Entity: ProductEntity)

    @Query("SELECT * from product ORDER BY productId DESC")
    fun productQty(): LiveData<List<ProductQtyModel>>

    @Query("SELECT * FROM stock LEFT JOIN product ON product.productId = stock.stockProductId WHERE product.productName LIKE '%' || :searchText || '%'")
    fun productStock(searchText: String): LiveData<List<ProductStockModel>>

    @Query("SELECT * FROM stock LEFT JOIN product ON product.productId = stock.stockProductId")
    fun productStockBackground(): List<ProductStockModel>

    @Query("SELECT * FROM stock LEFT JOIN price  ON price.priceList_productId = stock.stockProductId AND price.priceList_customerTypeId =:customerType LEFT JOIN product ON stockProductId = productId WHERE product.productName LIKE '%' || :searchText || '%' AND stock.stockProductQty > 0")
    fun productStockPrice(
        customerType: Int,
        searchText: String
    ): LiveData<List<ProductStockPriceModel>>

    // ============================================================================================= Price
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun priceInsert(Entity: PriceListEntity)

    // ============================================================================================= Stock
    @Query("SELECT SUM(stockProductQty) as value FROM stock")
    fun stockToday(): LiveData<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun stockInsert(Entity: StockEntity)

    @Query("UPDATE stock SET stockProductQty = stockProductQty - :stockOut  WHERE stockProductId = :productId")
    fun stockUpdate(productId: Int, stockOut: Int)

    // ============================================================================================= Transaction Header
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun transactionInsert(Entity: TransactionEntity): Long

    @Query("SELECT * FROM `transaction` WHERE transDate <= :endDate AND transDate >= :startDate ORDER BY transId DESC")
    fun transactionGetAll(startDate: String, endDate: String): LiveData<List<TransactionEntity>>

    @Query("SELECT COUNT(transId) FROM `transaction`")
    fun transactionCheckEmpty(): LiveData<Int>

    @Query("SELECT * FROM `transaction` WHERE transDate <= :endDateTime AND transDate >= :startDateTime ORDER BY transId DESC")
    fun transactionToday(
        startDateTime: String,
        endDateTime: String
    ): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM `transaction` WHERE transId=:transactionId")
    fun transactionGetById(transactionId: Int): LiveData<List<TransactionEntity>>

    @Query("UPDATE `transaction` SET transIsUpload = :isUpload  WHERE transId = :transactionId")
    fun transactionUpdateFlag(transactionId: Int, isUpload: Boolean)

    @Query("SELECT * FROM `transaction` WHERE transIsUpload=:isUpload")
    fun transactionLocal(isUpload: Boolean): LiveData<List<TransactionEntity>>

    // ============================================================================================= Transaction Detail
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun transactionDetailInsert(Entity: TransactionDetailEntity)

    @Query("SELECT * FROM transaction_detail WHERE transDetail_transId=:transactionId")
    fun transactionDetailGetByTransId(transactionId: Int): LiveData<List<TransactionDetailEntity>>

    @Query("SELECT SUM(transDetailQtyPCS) as productOutQty, SUM(transDetailSubTotal) as productOutMoney FROM transaction_detail WHERE transaction_detail.transDetailDate <= :endDateTime AND transaction_detail.transDetailDate >= :startDateTime")
    fun transactionDetailToday(startDateTime: String, endDateTime: String): LiveData<ProductOut>

    @Query("SELECT * FROM transaction_detail WHERE transDetail_transId=:transactionId")
    fun transactionDetailUpload(transactionId: Int): List<TransactionDetailEntity>

}