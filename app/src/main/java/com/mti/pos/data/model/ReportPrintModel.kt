package com.mti.pos.data.model

data class ReportPrintModel(
    val name: String,
    val productOut: String,
    val stock: String,
    val total: String,
)
