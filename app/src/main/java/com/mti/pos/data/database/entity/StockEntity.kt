package com.mti.pos.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "stock")
data class StockEntity (
    @PrimaryKey(autoGenerate = false)
    val stockProductId: Int,
    val stockProductQty: Int
)