package com.mti.pos.data.remote


import com.google.gson.annotations.SerializedName

data class TestModel(
    val message: String = "Ini pesan boss",


    @SerializedName("info")
    val info: Info? = Info(),
    @SerializedName("results")
    val results: List<Result?>? = listOf()
) {
    data class Info(
        @SerializedName("count")
        val count: Int? = 0,
        @SerializedName("pages")
        val pages: Int? = 0,
        @SerializedName("next")
        val next: String? = "",
        @SerializedName("prev")
        val prev: Any? = Any()
    )

    data class Result(
        @SerializedName("id")
        val id: Int? = 0,
        @SerializedName("name")
        val name: String? = "",
        @SerializedName("status")
        val status: String? = "",
        @SerializedName("species")
        val species: String? = ""
    )
}