package com.mti.pos.data.model

data class ProductStockModel (
    var productId: Int,
    var productCode: String,
    var productName: String,

    val stockProductId: Int?,
    val stockProductQty: Int?
)