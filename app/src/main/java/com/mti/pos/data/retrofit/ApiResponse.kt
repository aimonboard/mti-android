package com.mti.pos.data.retrofit

data class ApiResponse <out T>(val status: ApiStatus, val response: T?, val message: String?) {
    enum class ApiStatus {
        SUCCESS, ERROR
    }

    companion object {
        fun <T> success(response: T?): ApiResponse<T> {
            return ApiResponse(ApiStatus.SUCCESS, response,null)
        }

        fun <T> error(msg: String, response: T?): ApiResponse<T> {
            return ApiResponse(ApiStatus.ERROR, response, msg)
        }
    }
}