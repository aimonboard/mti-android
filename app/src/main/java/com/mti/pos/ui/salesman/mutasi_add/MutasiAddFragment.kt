package com.mti.pos.ui.salesman.mutasi_add

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentMutasiAddBinding
import com.mti.pos.ui.salesman.adapter.MutasiProductAddAdapter
import com.mti.pos.ui.salesman.adapter.MutasiProductViewAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_success.view.*

@AndroidEntryPoint
class MutasiAddFragment : Fragment(), MutasiProductViewAdapter.ProductItemListener {

    private lateinit var binding: FragmentMutasiAddBinding
    private lateinit var adapter: MutasiProductViewAdapter
    private val viewModel: MutasiAddViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMutasiAddBinding.inflate(inflater, container, false)
        adapter = MutasiProductViewAdapter(this)
        binding.recyclerProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        buttonListener()
    }

    override fun onProductDelete(product: ProductQtyModel) {
        viewModel.removeProduct(product)
    }

    private fun buttonListener() {
        binding.apply {
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            binding.btnMenu.setOnClickListener {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.openDrawer(GravityCompat.START)
            }

            btnAddProduct.setOnClickListener {
                productAddDialog()
            }

            btnSendMutasi.setOnClickListener {
                sendMutasi()
            }
        }
    }

    private fun setupObserver() {
        viewModel.getProfile().observe(viewLifecycleOwner, {
            viewModel.userId = it.first().userId
            viewModel.warehouseCode = it.first().warehouseCode
        })

        viewModel.productLive.observe(viewLifecycleOwner, {
            binding.hasProduct = !it.isNullOrEmpty()
            binding.btnSendMutasi.isEnabled = !it.isNullOrEmpty()
            adapter.setItems(it)
        })
    }

    private fun productAddDialog() {
        val dialog = Dialog(requireContext(), R.style.DialogAnimation)
        dialog.setContentView(R.layout.dialog_add_product)
        dialog.setCancelable(true)
        dialog.show()

        val btnClose = dialog.findViewById<TextView>(R.id.btn_close)
        val recycler = dialog.findViewById<RecyclerView>(R.id.recycler_product_add)
        val warningText = dialog.findViewById<TextView>(R.id.warning_text)
        val productAddAdapter = MutasiProductAddAdapter()
        recycler.adapter = productAddAdapter

        productAddAdapter.setListener(object : MutasiProductAddAdapter.ProductItemListener {
            override fun onProductAdd(product: ProductQtyModel) {
                viewModel.addProduct(product)
            }

            override fun onProductDelete(product: ProductQtyModel) {
                viewModel.removeProduct(product)
            }
        })

        viewModel.getProduct().observe(viewLifecycleOwner, Observer {
            productAddAdapter.setItems(it)
            if (it.isNotEmpty()) {
                warningText.visibility = View.GONE
            } else {
                warningText.visibility = View.VISIBLE
            }
        })

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun sendMutasi() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.sendMutasi().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    progressDialog.dismiss()
                    successDialog()
                }
                ApiResponse.ApiStatus.ERROR -> {
                    progressDialog.dismiss()
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun successDialog() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.dialog_success, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)

        dialogView.message.text = resources.getString(R.string.dialog_success)
        dialogView.btn_ok.setOnClickListener {
            alertDialog.dismiss()
            requireActivity().onBackPressed()
        }

        alertDialog.show()
    }

}