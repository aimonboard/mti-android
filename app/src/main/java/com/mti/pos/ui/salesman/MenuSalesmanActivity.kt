package com.mti.pos.ui.salesman

import android.Manifest
import android.app.AlertDialog
import android.content.*
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.mti.pos.R
import com.mti.pos.util.BluetoothUtil
import com.mti.pos.util.GpsUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_menu_salesman.*
import javax.inject.Inject
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

@AndroidEntryPoint
class MenuSalesmanActivity : AppCompatActivity() {

    companion object {
        var lat = ""
        var lon = ""
    }

    @Inject
    lateinit var bluetoothUtil: BluetoothUtil

    @Inject
    lateinit var gpsUtil: GpsUtil

    private lateinit var navController: NavController
    private val permissionRequestCode = 3543

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_salesman)
        setupNavigation()
        handleGPS()
    }

    override fun onResume() {
        super.onResume()
        gpsUtil.startLocationUpdates()
        bluetoothUtil.enableBluetooth(true)
    }

    override fun onPause() {
        super.onPause()
        gpsUtil.stopLocationUpdates()
        bluetoothUtil.enableBluetooth(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        gpsUtil.stopLocationUpdates()
        bluetoothUtil.enableBluetooth(false)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestCode) {
            if (grantResults.isNotEmpty()) {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsUtil.enableGps()
                } else {
                    handleRejectPermission()
                }
            }
        }
    }

    private fun setupNavigation() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        nav_view.setupWithNavController(navController)

        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> {
                    navController.navigate(R.id.nav_home, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_profile -> {
                    navController.navigate(R.id.nav_profile, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_cuti -> {
                    navController.navigate(R.id.nav_cuti, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }


                R.id.menu_customer -> {
                    navController.navigate(R.id.nav_customer, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_product -> {
                    navController.navigate(R.id.nav_product, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_mutasi -> {
                    navController.navigate(R.id.nav_mutasi, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_activity -> {
                    navController.navigate(R.id.nav_activity, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_transaction -> {
                    navController.navigate(R.id.nav_transaction, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_report -> {
                    navController.navigate(R.id.nav_report, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_setting -> {
                    navController.navigate(R.id.nav_setting, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                else -> {
                    return@setNavigationItemSelectedListener false
                }
            }
        }
    }

    private fun handleGPS() {
        if (!checkPermission()) {
            requestPermission();
        } else {
            gpsUtil.enableGps();
        }
    }

    private fun handleRejectPermission() {
        val dialog = AlertDialog.Builder(this)
        dialog.setCancelable(false)
        dialog.setMessage("Aplikasi membutuhkan akses lokasi")
        dialog.setPositiveButton("Izinkan") { _, _ ->
            requestPermission()
        }
        dialog.show()
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            permissionRequestCode
        )
    }

}