package com.mti.pos.ui.salesman.product

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {

    val searchText = MutableLiveData("")
    val productData = Transformations.switchMap(searchText) { search ->
        repository.getProduct(search)
    }

}