package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.data.model.ProductStockPriceModel
import com.mti.pos.databinding.ItemTransactionProductAddBinding
import com.mti.pos.util.Formater
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class TransProductAddAdapter : RecyclerView.Adapter<TransProductAddAdapter.ViewHolder>() {

    private var listener: ProductItemListener? = null
    private val items = ArrayList<ProductStockPriceModel>()

    fun setItems(items: List<ProductStockPriceModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemTransactionProductAddBinding = ItemTransactionProductAddBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    fun setListener(listener: ProductItemListener) {
        try {
            this.listener = listener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class ViewHolder(
        private val itemBinding: ItemTransactionProductAddBinding,
        private val listener: ProductItemListener?
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: ProductStockPriceModel) {
            itemBinding.productCode.text = item.productCode
            itemBinding.productName.text = item.productName
            itemBinding.productStock.text = "${item.stockProductQty} Pack"

            val priceString = "Rp ${Formater().idrFormat(item.priceListPrice ?: 0.0)} / Pack"
            itemBinding.productPrice.text = priceString

            // ===================================================================================== Qty
            val stock = item.stockProductQty ?: 0
            var qty = 0

            itemBinding.btnPlus.setOnClickListener {
                if (qty < stock) {
                    if (item.priceListPrice != null) {
                        qty += 1
                        item.stockProductQty = qty
                        itemBinding.productQty.text = qty.toString()
                        listener?.onProductAdd(item)
                    } else {
                        Toast.makeText(itemBinding.root.context, "Data tidak tersedia", Toast.LENGTH_LONG).show()
                    }
                }
            }

            itemBinding.btnMinus.setOnClickListener {
                if (qty > 0) {
                   qty -= 1
                   item.stockProductQty = qty
                   itemBinding.productQty.text = qty.toString()
                   listener?.onProductAdd(item)

                   if (qty == 0) {
                       listener?.onProductDelete(item)
                   }
                }
            }

        }
    }

    interface ProductItemListener {
        fun onProductAdd(product: ProductStockPriceModel)
        fun onProductDelete(product: ProductStockPriceModel)
    }

}