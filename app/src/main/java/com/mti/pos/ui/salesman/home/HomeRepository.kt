package com.mti.pos.ui.salesman.home

import android.bluetooth.BluetoothAdapter
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.*
import com.mti.pos.data.model.DeviceModel
import com.mti.pos.data.model.ProductOut
import com.mti.pos.data.model.ReportPrintModel
import com.mti.pos.data.remote.*
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import com.mti.pos.util.BluetoothUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashMap

@Singleton
class HomeRepository @Inject constructor(
    private val remoteDataSource: ApiService,
    private val localDataSource: AppDao,
    private val btAdapter: BluetoothAdapter,
    private val btUtil: BluetoothUtil
) {

    fun getLastSync(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun syncCustomer(userId: Int) {
        val params = HashMap<String, String>()
        params["salesman_id"] = userId.toString()

        remoteDataSource.customer(params).enqueue(object : Callback<CustomerRequest> {
            override fun onFailure(call: Call<CustomerRequest>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<CustomerRequest>,
                response: Response<CustomerRequest>
            ) {
                if (response.isSuccessful) {
                    GlobalScope.launch {
                        response.body()?.data?.forEach {
                            localDataSource.customerInsert(
                                CustomerEntity(
                                    customerId = it.customerId ?: 0,
                                    customerCode = it.customerCode ?: "",
                                    customerName = it.customerName ?: "",
                                    customerAddress = it.customerAddress ?: "",
                                    customer_customerTypeId = it.customerGroupId ?: 0,
                                    customerPhone = it.customerPhone ?: "",
                                    customerLat = it.customerLat ?: "",
                                    customerLon = it.customerLon ?: "",
                                    customerIsRegistered = it.customerIsRegistered ?: 0
                                )
                            )
                        }
                    }
                }
            }
        })
    }

    fun syncProduct(userId: Int) {
        val params = HashMap<String, String>()
        params["salesman_id"] = userId.toString()

        remoteDataSource.product(params).enqueue(object : Callback<ProductRequest> {
            override fun onFailure(call: Call<ProductRequest>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<ProductRequest>,
                response: Response<ProductRequest>
            ) {
                if (response.isSuccessful) {
                    GlobalScope.launch {
                        // Insert new data from server and update last sync
                        val productArray = mutableListOf<ProductEntity>()
                        var productIn = 0

                        response.body()?.data?.forEach {
                            productIn += it.stock

                            val productData = ProductEntity(
                                productId = it.itemId,
                                productCode = it.itemCode,
                                productName = it.itemName
                            )

                            productArray.add(productData)
                            localDataSource.productInsert(productData)
                        }
                    }
                }
            }
        })
    }

    fun syncStock(userId: Int, date: String) {
        val params = HashMap<String, String>()
        params["salesman_id"] = userId.toString()
        params["stock_date"] = "$date 10:00:00"

        remoteDataSource.stock(params).enqueue(object : Callback<StockRequest> {
            override fun onFailure(call: Call<StockRequest>, t: Throwable) {

            }

            override fun onResponse(call: Call<StockRequest>, response: Response<StockRequest>) {
                if (response.isSuccessful) {
                    GlobalScope.launch {
                        // Insert stock
                        response.body()?.data?.forEach {
                            localDataSource.stockInsert(
                                StockEntity(
                                    stockProductId = it?.iditem?.toInt() ?: 0,
                                    stockProductQty = it?.stockIn?.toInt() ?: 0
                                )
                            )
                        }

                        // Update last sync
                        localDataSource.profileUpdateLastSync(userId, date)
                    }
                }
            }
        })
    }

    fun syncPrice() {
        remoteDataSource.price().enqueue(object : Callback<PriceRequest> {
            override fun onFailure(call: Call<PriceRequest>, t: Throwable) {

            }

            override fun onResponse(call: Call<PriceRequest>, response: Response<PriceRequest>) {
                if (response.isSuccessful) {
                    GlobalScope.launch {
                        response.body()?.data?.forEach { groupData ->
//                            localDataSource.customerTypeInsert(
//                                CustomerTypeEntity(
//                                    customerTypeId = groupData.groupId,
//                                    customerTypeCode = groupData.groupCode,
//                                    customerTypeName = groupData.groupName,
//                                )
//                            )

                            groupData.priceList.forEach { priceData ->
                                localDataSource.priceInsert(
                                    PriceListEntity(
                                        priceListId = priceData.priceId,
                                        priceList_customerTypeId = groupData.groupId,
                                        priceList_productId = priceData.itemId,
                                        priceListMeasurement = priceData.measurement,
                                        priceListPrice = priceData.price,
                                        priceListStartDate = priceData.startDate,
                                        priceListEndDate = priceData.endDate
                                    )
                                )
                            }
                        }
                    }
                }
            }
        })
    }

    fun getStockToday(): LiveData<Int> {
        return localDataSource.stockToday()
    }

    fun getTransactionToday(
        startDateTime: String,
        endDateTime: String
    ): LiveData<List<TransactionEntity>> {
        return localDataSource.transactionToday(
            startDateTime = startDateTime,
            endDateTime = endDateTime
        )
    }

    fun getTransactionDetailToday(
        startDateTime: String,
        endDateTime: String
    ): LiveData<ProductOut> {
        return localDataSource.transactionDetailToday(
            startDateTime = startDateTime,
            endDateTime = endDateTime
        )
    }

    fun checkTransactionLocal(): LiveData<List<TransactionEntity>> {
        return localDataSource.transactionLocal(false)
    }

    fun uploadTransactionLocal(transactionHeader: List<TransactionEntity>): LiveData<ApiResponse<TransactionRequest>> {
        val responseData = MutableLiveData<ApiResponse<TransactionRequest>>()

        GlobalScope.launch {
            remoteDataSource.transaction(generateBody(transactionHeader))
                .enqueue(object : Callback<TransactionRequest> {
                    override fun onFailure(call: Call<TransactionRequest>, t: Throwable) {
                        responseData.postValue(
                            ApiResponse.error(
                                "Error ${t.message}",
                                null
                            )
                        )
                    }

                    override fun onResponse(
                        call: Call<TransactionRequest>,
                        response: Response<TransactionRequest>
                    ) {
                        responseData.postValue(
                            ApiResponse.success(
                                response.body()
                            )
                        )

                        updateFlagTransaction(transactionHeader)
                    }
                })
        }

        return responseData
    }

    private fun generateBody(transactionHeader: List<TransactionEntity>): RequestBody {
        val bodyArray = JSONArray()

        transactionHeader.forEach { header ->
            val mainObject = JSONObject()
            mainObject.put("invoice_code", header.transCode)
            mainObject.put("invoice_date", header.transDate)
            mainObject.put("salesman_id", header.trans_salesId)
            mainObject.put("customer_id", header.trans_customerId)
            mainObject.put("latitude", header.transLatitude)
            mainObject.put("longitude", header.transLongitude)

            val detailArray = JSONArray()
            localDataSource.transactionDetailUpload(header.transId).forEach { detail ->
                val detailObject = JSONObject()
                detailObject.put("item_id", detail.transDetail_productId)
                detailObject.put("item_name", detail.transDetail_productName)
                detailObject.put("qty", detail.transDetailQtyPCS)
                detailObject.put("measurement", "Pack")
                detailObject.put("unit_price", detail.transDetailPrice)
                detailArray.put(detailObject)
            }

            mainObject.put("detail", detailArray)
            bodyArray.put(mainObject)
        }

        Log.e("aim", "body : $bodyArray")

        return RequestBody.create(
            MediaType.parse("application/json; charset=utf-8"),
            bodyArray.toString()
        )
    }

    private fun updateFlagTransaction(transactionHeader: List<TransactionEntity>) {
        GlobalScope.launch {
            transactionHeader.forEach {
                localDataSource.transactionUpdateFlag(
                    transactionId = it.transId,
                    isUpload = true
                )
            }
        }
    }

    fun getPairedDevice(): List<DeviceModel> {
        val pairedData = ArrayList<DeviceModel>()
        btAdapter.bondedDevices.forEach {
            pairedData.add(
                DeviceModel(
                    deviceName = it.name,
                    deviceAddress = it.address,
                )
            )
        }
        return pairedData
    }

    fun printDailyReport(data: ReportPrintModel) {
        GlobalScope.launch {
            val stock = localDataSource.productStockBackground()
            btUtil.printDailyReport(data, stock)
        }
    }

}