package com.mti.pos.ui.salesman.cuti

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mti.pos.R
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentCutiBinding
import com.mti.pos.ui.salesman.adapter.CutiAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CutiFragment : Fragment() {

    private lateinit var binding: FragmentCutiBinding
    private lateinit var adapter: CutiAdapter
    private val viewModel: CutiViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCutiBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        adapter = CutiAdapter()
        binding.recyclerCuti.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        buttonListener()
    }

    private fun buttonListener() {
        binding.refreshLayout.setOnRefreshListener {
            setupObserver()
        }

        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnCuti.setOnClickListener {
            findNavController().navigate(
                R.id.nav_cuti_request
            )
        }
    }

    private fun setupObserver() {
        binding.refreshLayout.isRefreshing = true

        viewModel.getProfile().observe(viewLifecycleOwner, { profile ->

            viewModel.getCuti(profile.first().userId).observe(viewLifecycleOwner, Observer {
                binding.refreshLayout.isRefreshing = false
                when (it.status) {
                    ApiResponse.ApiStatus.SUCCESS -> {
                        binding.hasCustomer = !it.response?.data.isNullOrEmpty()
                        adapter.submitList(it.response?.data)
                    }
                    ApiResponse.ApiStatus.ERROR -> {
                        Toast.makeText(
                            requireContext(),
                            it.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        })

    }

}