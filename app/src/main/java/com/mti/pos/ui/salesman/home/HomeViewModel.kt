package com.mti.pos.ui.salesman.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.data.model.DeviceModel
import com.mti.pos.data.model.ProductOut
import com.mti.pos.data.model.ReportPrintModel
import com.mti.pos.data.remote.TransactionRequest
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: HomeRepository
) : ViewModel() {

    val profileName = MutableLiveData("")
    val profileDate = MutableLiveData("")

    var productStockInt = 0
    val productStock = MutableLiveData("")

    var productOutQtyInt = 0
    var productOutMoneyDouble = 0.0
    val productOutMoney = MutableLiveData("")


    fun getLastSync(): LiveData<List<ProfileEntity>> {
        return repository.getLastSync()
    }

    fun doSync(profileData: List<ProfileEntity>) {
        if (profileData.isNotEmpty()) {
            val syncFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val uiFormat = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
            val now = syncFormat.format(Date())

            val userId = profileData.first().userId
            val lastSync = profileData.first().lastSync
            profileName.value = profileData.first().userName
            profileDate.value = uiFormat.format(Date())
            repository.syncStock(userId, now)

            if (lastSync != now) {
                // Get data from server every day
                Log.e("aim", "lastSync : $lastSync, now : $now")
                repository.syncCustomer(userId)
                repository.syncProduct(userId)
                repository.syncPrice()
                repository.syncStock(userId, now)
            }
        }
    }

    fun getStockToday(): LiveData<Int> {
        return repository.getStockToday()
    }

    fun getTransactionToday(
        startDateTime: String,
        endDateTime: String
    ): LiveData<List<TransactionEntity>> {
        return repository.getTransactionToday(
            startDateTime = startDateTime,
            endDateTime = endDateTime
        )
    }

    fun getTransactionDetailToday(
        startDateTime: String,
        endDateTime: String
    ): LiveData<ProductOut> {
        return repository.getTransactionDetailToday(
            startDateTime = startDateTime,
            endDateTime = endDateTime
        )
    }

    fun getStartDay(): String {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val now = format.format(Date()).split(" ")
        return "${now[0]} 00:01:00"
    }

    fun getEndDay(): String {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val now = format.format(Date()).split(" ")
        return "${now[0]} 23:59:00"
    }

    fun checkTransactionLocal(): LiveData<List<TransactionEntity>> {
        return repository.checkTransactionLocal()
    }

    fun uploadTransactionLocal(data: List<TransactionEntity>) : LiveData<ApiResponse<TransactionRequest>> {
        return repository.uploadTransactionLocal(data)
    }

    fun getPairedDevice(): List<DeviceModel> {
        return repository.getPairedDevice()
    }

    fun printDailyReport() {
        repository.printDailyReport(
            ReportPrintModel(
                name = profileName.value ?: "",
                productOut = "$productOutQtyInt Packs",
                stock = productStock.value ?: "",
                total = productOutMoney.value ?: ""
            )
        )
    }
}

