package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.databinding.ItemMutasiProductAddBinding

class MutasiProductAddAdapter : RecyclerView.Adapter<MutasiProductAddAdapter.ViewHolder>() {

    private var listener: ProductItemListener? = null
    private val items = ArrayList<ProductQtyModel>()

    fun setItems(items: List<ProductQtyModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMutasiProductAddBinding =
            ItemMutasiProductAddBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    fun setListener(listener: ProductItemListener) {
        try {
            this.listener = listener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class ViewHolder(
        private val itemBinding: ItemMutasiProductAddBinding,
        private val listener: ProductItemListener?
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: ProductQtyModel) {
            itemBinding.productCode.text = item.productCode
            itemBinding.productName.text = item.productName

            // ===================================================================================== Qty
            var qty = 0

            itemBinding.btnPlus.setOnClickListener {
                qty += 1
                item.productQty = qty
                itemBinding.productQty.text = qty.toString()
                listener?.onProductAdd(item)
            }

            itemBinding.btnMinus.setOnClickListener {
                if (qty > 0) {
                    qty -= 1
                    item.productQty = qty
                    itemBinding.productQty.text = qty.toString()
                    listener?.onProductAdd(item)

                    if (qty == 0) {
                        listener?.onProductDelete(item)
                    }
                }
            }

        }
    }

    interface ProductItemListener {
        fun onProductAdd(product: ProductQtyModel)
        fun onProductDelete(product: ProductQtyModel)
    }

}