package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.databinding.ItemReportBinding
import com.mti.pos.util.Formater
import java.text.SimpleDateFormat
import java.util.*

class ReportAdapter(private val listener: ReportItemListener) :
    ListAdapter<TransactionEntity, ReportAdapter.ViewHolder>(ReportDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_report,
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(position, itemCount, getItem(position))

    class ViewHolder(
        private val itemBinding: ItemReportBinding,
        private val listener: ReportItemListener
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, itemCount: Int, item: TransactionEntity) {
            val localeID = Locale("in", "ID")
            val uiFormat = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault())
            val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

            itemBinding.productCode.text = item.transCode

            val dateObj = serverFormat.parse(item.transDate)
            itemBinding.productName.text = uiFormat.format(dateObj!!)

            val priceString = "Rp ${Formater().idrFormat(item.transGrandTotal)}"
            itemBinding.productTotal.text = priceString

            if (position + 1 == itemCount) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }

            itemBinding.root.setOnClickListener {
                listener.onItemClick(item.transId)
            }

        }

    }

    private class ReportDiffUtil : DiffUtil.ItemCallback<TransactionEntity>() {
        override fun areItemsTheSame(
            oldItem: TransactionEntity,
            newItem: TransactionEntity
        ): Boolean {
            return oldItem.transId == newItem.transId
        }

        override fun areContentsTheSame(
            oldItem: TransactionEntity,
            newItem: TransactionEntity
        ): Boolean {
            return oldItem == newItem
        }

    }

    interface ReportItemListener {
        fun onItemClick(transactionId: Int)
    }

}