package com.mti.pos.ui.salesman.setting

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.model.DeviceModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SettingViewModel @Inject constructor(
    private val repository: SettingRepository
) : ViewModel() {

    var pairedDevice = MutableLiveData<List<DeviceModel>>()
    var newDevice = MutableLiveData<DeviceModel>()

    fun getPairDevice() {
        pairedDevice.value = repository.getPairDevice()
    }

    fun startScanBluetooth() {
        val btAdapter = BluetoothAdapter.getDefaultAdapter()
        btAdapter.startDiscovery()
    }

    fun stopScanBluetooth() {
        val btAdapter = BluetoothAdapter.getDefaultAdapter()
        btAdapter.cancelDiscovery()
    }

    fun scanBluetoothResult(intent: Intent) {
        val foundDevice = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
        if (foundDevice != null) {
            val strength = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE)
            newDevice.value = DeviceModel(
                deviceName = foundDevice.name,
                deviceAddress = foundDevice.address,
            )
        }
    }

    fun pairNewBluetooth(deviceModel: DeviceModel) {
        repository.pairDevice(deviceModel)
        getPairDevice()
    }



}