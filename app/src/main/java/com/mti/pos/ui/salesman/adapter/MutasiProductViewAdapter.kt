package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.databinding.ItemMutasiProductViewBinding

class MutasiProductViewAdapter(private val listener: ProductItemListener) :
    RecyclerView.Adapter<MutasiProductViewAdapter.ViewModel>() {

    private var items = ArrayList<ProductQtyModel>()

    fun setItems(items: ArrayList<ProductQtyModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getAllItems() = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val binding =
            ItemMutasiProductViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewModel(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) =
        holder.bind(position, items[position])

    inner class ViewModel(
        private val itemBinding: ItemMutasiProductViewBinding,
        private val listener: ProductItemListener
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, item: ProductQtyModel) {
            itemBinding.productCode.text = item.productCode
            itemBinding.productName.text = item.productName

            val productQty = "${item.productQty} Packs"
            itemBinding.productTotal.text = productQty

            if (position == items.size - 1) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }

            itemBinding.btnDelete.setOnClickListener {
                items.remove(item)
                listener.onProductDelete(item)
            }

        }
    }

    interface ProductItemListener {
        fun onProductDelete(product: ProductQtyModel)
    }

}