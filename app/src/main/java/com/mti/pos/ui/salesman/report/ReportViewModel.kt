package com.mti.pos.ui.salesman.report

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.TransactionEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ReportViewModel @Inject constructor(
    private val repository: ReportRepository
): ViewModel() {

    private var startDate = ""
    private var endDate = ""

    fun getHeaderTransaction(): LiveData<List<TransactionEntity>> {
        return repository.getHeaderTransaction(startDate, endDate)
    }

    fun selectedSpinner(position: Int) {
        val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val cal = Calendar.getInstance()

        when (position) {
            0 -> {
                // this day
                val now = serverFormat.format(Date()).split(" ")
                startDate = "${now[0]} 00:01:00"
                endDate = "${now[0]} 23:59:00"
            }
            1 -> {
                // this week start
                cal[Calendar.DAY_OF_WEEK] = cal.firstDayOfWeek
                val startString = serverFormat.format(cal.time).split(" ")
                startDate = "${startString[0]} 00:01:00"

                // this week end
                cal.add(Calendar.WEEK_OF_YEAR, 1)
                cal.add(Calendar.DATE, -1)
                val endString = serverFormat.format(cal.time).split(" ")
                endDate = "${endString[0]} 23:59:00"
            }
            2 -> {
                // this month start
                cal.set(Calendar.DAY_OF_MONTH, 1)
                val startString = serverFormat.format(cal.time).split(" ")
                startDate = "${startString[0]} 00:01:00"

                // this month end
                cal.add(Calendar.MONTH, 1)
                cal.add(Calendar.DATE, -1)
                val endString = serverFormat.format(cal.time).split(" ")
                endDate = "${endString[0]} 23:59:00"
            }
            3 -> {
                // this year start
                cal.set(Calendar.DAY_OF_YEAR, 1)
                val startString = serverFormat.format(cal.time).split(" ")
                startDate = "${startString[0]} 00:01:00"

                // this year end
                cal.add(Calendar.YEAR, 1)
                cal.add(Calendar.DATE, -1)
                val endString = serverFormat.format(cal.time).split(" ")
                endDate = "${endString[0]} 23:59:00"

            }
        }

        Log.e("aim","filter start date : $startDate")
        Log.e("aim","filter end date : $endDate")

    }

}