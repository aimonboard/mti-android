package com.mti.pos.ui.salesman.customer

import androidx.lifecycle.LiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.CustomerEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomerRepository @Inject constructor(
    private val localDataSource: AppDao
) {

    fun getCustomer(searchText: String): LiveData<List<CustomerEntity>> {
        return localDataSource.customerGetAll(searchText)
    }

}