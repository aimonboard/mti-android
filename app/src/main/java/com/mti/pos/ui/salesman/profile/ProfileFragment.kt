package com.mti.pos.ui.salesman.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.mti.pos.MainActivity
import com.mti.pos.R
import com.mti.pos.databinding.FragmentProfileBinding
import com.mti.pos.util.EnumUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val viewModel: ProfileViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        buttonListener()

    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnOut.setOnClickListener {
            viewModel.logout()

            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            activity?.finish()
        }
    }

    private fun setupObserver() {
        viewModel.getProfile().observe(viewLifecycleOwner, Observer {
            binding.nameValue.text = it.first().userName
            binding.codeValue.text = it.first().warehouseCode
            binding.positionValue.text =
                if (it.first().userType == EnumUtil.UserType.Salesman.code) {
                    EnumUtil.UserType.Salesman.name
                } else {
                    EnumUtil.UserType.Supervisor.name
                }
        })
    }

}