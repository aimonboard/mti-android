package com.mti.pos.ui.salesman.customer

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CustomerViewModel @Inject constructor(
    private val repository: CustomerRepository
) : ViewModel() {

    val searchText = MutableLiveData("")
    val customerData = Transformations.switchMap(searchText) { search ->
        repository.getCustomer(search)
    }

}