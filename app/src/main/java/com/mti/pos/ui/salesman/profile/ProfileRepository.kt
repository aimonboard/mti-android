package com.mti.pos.ui.salesman.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.AppDatabase
import com.mti.pos.data.database.entity.ProfileEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProfileRepository @Inject constructor(
    private val localDataSource: AppDao,
    private val appDatabase: AppDatabase
) {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun logout() {
        GlobalScope.launch {
            appDatabase.clearAllTables()
        }
    }

}