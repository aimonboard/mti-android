package com.mti.pos.ui.salesman.cuti_form

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CutiFormViewModel @Inject constructor(
    private val repository: CutiFormRepository
) : ViewModel() {

    val userId = MutableLiveData("")
    val userName = MutableLiveData("")

    private val spinnerType = MutableLiveData("")

    var dateChoose: Date? = null
    val dateError = MutableLiveData("")
    val dateStartUi = MutableLiveData("")
    private val dateStartServer = MutableLiveData("")
    private val dateEndServer = MutableLiveData("")

    val desc = MutableLiveData("")

    var durationInt = 0
    val duration = MutableLiveData("${durationInt + 1}")

    fun selectedDate() {
        if (dateChoose!!.before(Date()) || dateChoose!! == Date()) {
            dateError.value = "Cuti diajukan untuk masa yang akan datang"
            dateStartUi.value = ""
        } else {
            dateError.value = ""
            val uiFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            dateStartUi.value = uiFormat.format(dateChoose!!.time)

            val serverFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            dateStartServer.value = serverFormat.format(dateChoose!!.time)
            dateEndServer.value = serverFormat.format(dateChoose!!.time)
        }

        Log.e("aim", "start : ${dateStartServer.value} end : ${dateEndServer.value}")
    }

    fun selectedSpinner(pos: Int) {
        spinnerType.value = "${pos + 1}"
    }

    fun durationPlus() {
        durationInt += 1
        duration.value = "${durationInt + 1}"

        if (dateChoose != null) {
            val mCalendar = Calendar.getInstance()
            mCalendar.time = dateChoose!!
            mCalendar.add(Calendar.DATE, durationInt)

            val serverFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            dateStartServer.value = serverFormat.format(dateChoose!!)
            dateEndServer.value = serverFormat.format(mCalendar.time)
        }
    }

    fun durationMinus() {
        if (durationInt > 0) {
            durationInt -= 1
            duration.value = "${durationInt + 1}"

            if (dateChoose != null) {
                val mCalendar = Calendar.getInstance()
                mCalendar.time = dateChoose!!
                mCalendar.add(Calendar.DATE, durationInt)

                val serverFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                dateStartServer.value = serverFormat.format(dateChoose!!)
                dateEndServer.value = serverFormat.format(mCalendar.time)
            }
        }
    }

    fun getProfile() : LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun sendCuti(): LiveData<ApiResponse<ResponseBody>> {
        return repository.sendCuti(
            userId = userId.value ?: "",
            type = spinnerType.value ?: "",
            startDate = dateStartServer.value ?: "",
            endDate = dateEndServer.value ?: "",
            duration = duration.value ?: "",
            desc = desc.value ?: ""
        )
    }

}