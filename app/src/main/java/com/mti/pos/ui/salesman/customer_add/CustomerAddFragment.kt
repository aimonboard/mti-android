package com.mti.pos.ui.salesman.customer_add

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.mti.pos.R
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentCustomerAddBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_success.view.*

@AndroidEntryPoint
class CustomerAddFragment : Fragment() {

    private lateinit var binding: FragmentCustomerAddBinding

    private val viewModel: CustomerAddViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCustomerAddBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readArgument()
        readLocationResult()
        buttonListener()
        setupObservers()
    }

    private fun readArgument() {
        viewModel.customerId = arguments?.getInt("customer_id") ?: 0
        binding.headerName.text = if (viewModel.customerId == 0) {
            "Tambahkan Pelanggan"
        } else {
            "Ubah Pelanggan"
        }

        viewModel.customerName.value = arguments?.getString("customer_name") ?: ""
        viewModel.customerPhone.value = arguments?.getString("customer_phone") ?: ""
        viewModel.customerAddress.value = arguments?.getString("customer_address") ?: ""

        viewModel.customerLat = arguments?.getString("customer_lat") ?: ""
        viewModel.customerLon = arguments?.getString("customer_lon") ?: ""
    }

    private fun readLocationResult() {
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<String>("address")
            ?.observe(viewLifecycleOwner) {
                val dataArray = it.split("|0|")
                viewModel.customerAddress.value = dataArray[0]
                viewModel.customerLat = dataArray[1]
                viewModel.customerLon = dataArray[2]
                Log.e("aim", "data alamat : $it")
            }
    }

    private fun buttonListener() {
        binding.btnBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.btnLocation.setOnClickListener {
            findNavController().navigate(
                R.id.nav_customer_add_maps,
                bundleOf(
                    "customer_lat" to viewModel.customerLat,
                    "customer_lon" to viewModel.customerLon
                )
            )
        }
        binding.btnAddCustomer.setOnClickListener {
            if (viewModel.customerId == 0) {
                customerAdd()
            } else {
                customerEdit()
            }
        }
    }

    private fun setupObservers() {
        viewModel.getProfile().observe(viewLifecycleOwner, {
            viewModel.userId = it.first().userId
        })

        binding.nameValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                viewModel.customerName.value = binding.nameValue.text.toString()
            }
        })

        binding.phoneValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                viewModel.customerPhone.value = binding.phoneValue.text.toString()
            }
        })

        binding.addressValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                viewModel.customerAddress.value = binding.addressValue.text.toString()
            }
        })
    }

    private fun customerAdd() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.customerAdd().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    progressDialog.dismiss()
                    successDialog()
                }
                ApiResponse.ApiStatus.ERROR -> {
                    progressDialog.dismiss()
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun customerEdit() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.customerEdit().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    progressDialog.dismiss()
                    successDialog()
                }
                ApiResponse.ApiStatus.ERROR -> {
                    progressDialog.dismiss()
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun successDialog() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.dialog_success, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)

        dialogView.message.text = resources.getString(R.string.dialog_success)
        dialogView.btn_ok.setOnClickListener {
            alertDialog.dismiss()
            requireActivity().onBackPressed()
        }

        alertDialog.show()
    }

}