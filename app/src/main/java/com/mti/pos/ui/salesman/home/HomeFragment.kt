package com.mti.pos.ui.salesman.home

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mti.pos.R
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentHomeBinding
import com.mti.pos.ui.salesman.adapter.ReportAdapter
import com.mti.pos.util.Formater
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), ReportAdapter.ReportItemListener {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: ReportAdapter
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        adapter = ReportAdapter(this)
        binding.recyclerProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        buttonListener()

        uploadLocalTransactions(false)
    }

    override fun onItemClick(transactionId: Int) {
        val bundle = Bundle()
        bundle.putInt("transaction_id", transactionId)
        findNavController().navigate(R.id.nav_report_detail, bundle)
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnPrint.setOnClickListener {
//            selectDeviceDialog()
            uploadLocalTransactions(true)
        }
    }

    private fun setupObservers() {
        viewModel.getLastSync().observe(viewLifecycleOwner, Observer {
            viewModel.doSync(it)
        })

        val startDay = viewModel.getStartDay()
        val endDay = viewModel.getEndDay()

        viewModel.getStockToday().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.productStockInt = it
                viewModel.productStock.value = "$it Packs"
            }
        })

        viewModel.getTransactionDetailToday(
            startDateTime = startDay,
            endDateTime = endDay
        ).observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.productOutQtyInt = it.productOutQty
                viewModel.productOutMoneyDouble = it.productOutMoney

                val outPacks =
                    if (it.productOutQty > 0) "${it.productOutQty} Packs - "
                    else ""
                val outMoney = "Rp ${Formater().idrFormat(it.productOutMoney)}"

                viewModel.productOutMoney.value = "$outPacks$outMoney"
            }
        })

        viewModel.getTransactionToday(
            startDateTime = startDay,
            endDateTime = endDay
        ).observe(viewLifecycleOwner, Observer {
            binding.hasProduct = !it.isNullOrEmpty()
            adapter.submitList(it)
        })
    }

    private fun uploadLocalTransactions(printReport: Boolean) {
        viewModel.checkTransactionLocal().observe(viewLifecycleOwner, Observer { trans ->
            // check the database if there are transactions that have not been uploaded
            if (trans.isNotEmpty()) {
                val progressDialog = ProgressDialog(requireContext())
                progressDialog.setMessage("Mengirim data")
                progressDialog.setCancelable(false)
                progressDialog.show()

                viewModel.uploadTransactionLocal(trans).observe(viewLifecycleOwner, Observer {
                    when (it.status) {
                        ApiResponse.ApiStatus.SUCCESS -> {
                            progressDialog.dismiss()
                            if (printReport) viewModel.printDailyReport()
                        }
                        ApiResponse.ApiStatus.ERROR -> {
                            progressDialog.dismiss()
                            if (printReport) {
                                Toast.makeText(
                                    requireContext(),
                                    "Perangkat harus terhubung dengan internet",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                })
            } else {
                // All transactions uploaded, print report
                if (printReport) printDailyReport()
            }
        })
    }

    private fun printDailyReport() {
        if (adapter.itemCount > 0) {
            viewModel.printDailyReport()
        } else {
            Toast.makeText(
                requireContext(),
                "Tidak ada transaksi",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun selectDeviceDialog() {
        val data = viewModel.getPairedDevice()
        val deviceName = Array(data.size) { "" }
        data.forEachIndexed { index, it ->
            deviceName[index] = it.deviceName
        }

        val builder = AlertDialog.Builder(requireContext())
        builder.setCancelable(true)
        builder.setTitle("Pilih Printer").setItems(deviceName) { _, which ->
            viewModel.printDailyReport()
        }

        builder.create()
        builder.show()
    }

}