package com.mti.pos.ui.salesman.product

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import com.mti.pos.R
import com.mti.pos.databinding.FragmentProductBinding
import com.mti.pos.ui.salesman.adapter.ProductAdapter
import com.mti.pos.util.KeyboardHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductFragment : Fragment() {

    private lateinit var binding: FragmentProductBinding
    private lateinit var adapter: ProductAdapter
    private val viewModel: ProductViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        adapter = ProductAdapter()
        binding.recyclerProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        buttonListener()
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnSearch.setOnClickListener {
            if (binding.headerName.visibility == View.VISIBLE) {
                binding.headerName.visibility = View.GONE
                binding.searchLay.visibility = View.VISIBLE
                binding.btnSearch.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(requireContext(), binding.searchValue)
            } else {
                binding.headerName.visibility = View.VISIBLE
                binding.searchLay.visibility = View.GONE
                binding.btnSearch.setImageResource(R.drawable.ic_search)

                binding.searchValue.setText("")
                KeyboardHelper.hideSoftKeyboard(requireContext(), binding.searchValue)
            }
        }
    }

    private fun setupObservers() {
        viewModel.productData.observe(viewLifecycleOwner, Observer {
            binding.hasProduct = !it.isNullOrEmpty()
            adapter.submitList(it)
        })
    }



}