package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.remote.CutiRequest
import com.mti.pos.databinding.ItemCutiBinding

class CutiAdapter: ListAdapter<CutiRequest.Data, CutiAdapter.ViewHolder>(CustomerDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_cuti,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    class ViewHolder(private val itemBinding: ItemCutiBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: CutiRequest.Data) {
            itemBinding.statusValue.text = item.statusName
            itemBinding.statusValue.setBackgroundResource(
                when(item.status) {
                    "0" -> R.drawable.shape_button_orange
                    "3" -> R.drawable.shape_button_red
                    else -> R.drawable.shape_button_orange
                }
            )

            itemBinding.typeValue.text = item.leaveTypeName
            itemBinding.requestDateValue.text = item.requestDate

            if (item.startDate == item.endDate) {
                itemBinding.cutiDateValue.text = item.startDate
            } else {
                itemBinding.cutiDateValue.text = "${item.startDate} - ${item.endDate}"
            }

            itemBinding.descValue.text = item.notes

        }

    }

    private class CustomerDiffUtil : DiffUtil.ItemCallback<CutiRequest.Data>() {
        override fun areItemsTheSame(oldItem: CutiRequest.Data, newItem: CutiRequest.Data): Boolean {
            return oldItem.requestDate == newItem.requestDate
        }

        override fun areContentsTheSame(oldItem: CutiRequest.Data, newItem: CutiRequest.Data): Boolean {
            return oldItem == newItem
        }

    }

}