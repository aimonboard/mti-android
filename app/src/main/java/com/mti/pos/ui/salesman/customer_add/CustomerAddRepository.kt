package com.mti.pos.ui.salesman.customer_add

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.CustomerRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CustomerAddRepository @Inject constructor(
    val remoteDataSource: ApiService,
    val localDataSource: AppDao
) {

    fun getProfile(): MutableLiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun customerAdd(
        userId: Int,
        customerName: String,
        customerAddress: String,
        customerPhone: String,
        customerLat: String,
        customerLon: String
    ): MutableLiveData<ApiResponse<CustomerRequest>> {
        val responseData = MutableLiveData<ApiResponse<CustomerRequest>>()

        val params = HashMap<String, String>()
        params["salesman_id"] = userId.toString()
        params["customer_name"] = customerName
        params["customer_address"] = customerAddress
        params["customer_phone"] = customerPhone
        params["customer_lat"] = customerLat
        params["customer_lon"] = customerLon

        remoteDataSource.customerAdd(params).enqueue(object : Callback<CustomerRequest> {
            override fun onFailure(call: Call<CustomerRequest>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(
                call: Call<CustomerRequest>,
                response: Response<CustomerRequest>
            ) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )

                    response.body()?.data?.first()?.let { insertCustomer(it) }
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

    fun customerEdit(
        customerId: Int,
        customerName: String,
        customerAddress: String,
        customerPhone: String,
        customerLat: String,
        customerLon: String
    ): MutableLiveData<ApiResponse<CustomerRequest>> {
        val responseData = MutableLiveData<ApiResponse<CustomerRequest>>()

        val params = HashMap<String, String>()
        params["customer_id"] = customerId.toString()
        params["customer_name"] = customerName
        params["customer_address"] = customerAddress
        params["customer_phone"] = customerPhone
        params["customer_lat"] = customerLat
        params["customer_lon"] = customerLon

        Log.e("aim", "body edit : $params")

        remoteDataSource.customerEdit(params).enqueue(object : Callback<CustomerRequest> {
            override fun onFailure(call: Call<CustomerRequest>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(
                call: Call<CustomerRequest>,
                response: Response<CustomerRequest>
            ) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )

                    response.body()?.data?.first()?.let { insertCustomer(it) }
                    Log.e("aim", "response edit : ${response.body()?.data}")
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

    private fun insertCustomer(responseData: CustomerRequest.Data) {
        GlobalScope.launch {
            localDataSource.customerInsert(
                CustomerEntity(
                    customerId = responseData.customerId ?: 0,
                    customerCode = responseData.customerCode ?: "",
                    customerName = responseData.customerName ?: "",
                    customerAddress = responseData.customerAddress ?: "",
                    customer_customerTypeId = responseData.customerGroupId ?: 0,
                    customerPhone = responseData.customerPhone ?: "",
                    customerLat = responseData.customerLat ?: "",
                    customerLon = responseData.customerLon ?: "",
                    customerIsRegistered = responseData.customerIsRegistered ?: 0,
                )
            )
        }
    }

}