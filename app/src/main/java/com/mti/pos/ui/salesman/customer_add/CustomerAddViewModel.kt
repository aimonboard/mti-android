package com.mti.pos.ui.salesman.customer_add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.CustomerRequest
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CustomerAddViewModel @Inject constructor(
    private val repository: CustomerAddRepository
) : ViewModel() {

    val customerName = MutableLiveData("")
    val customerAddress = MutableLiveData("")
    val customerPhone = MutableLiveData("")

    var userId = 0
    var customerId = 0
    var customerLat = ""
    var customerLon = ""

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun customerAdd(): LiveData<ApiResponse<CustomerRequest>> {
        return repository.customerAdd(
            userId = userId,
            customerName = customerName.value.toString(),
            customerAddress = customerAddress.value.toString(),
            customerPhone = customerPhone.value.toString(),
            customerLat = customerLat,
            customerLon = customerLon
        )
    }

    fun customerEdit(): LiveData<ApiResponse<CustomerRequest>> {
        return repository.customerEdit(
            customerId = customerId,
            customerName = customerName.value.toString(),
            customerAddress = customerAddress.value.toString(),
            customerPhone = customerPhone.value.toString(),
            customerLat = customerLat,
            customerLon = customerLon
        )
    }

}