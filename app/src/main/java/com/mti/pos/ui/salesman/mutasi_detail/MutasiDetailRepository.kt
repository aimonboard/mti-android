package com.mti.pos.ui.salesman.mutasi_detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MutasiDetailRepository @Inject constructor(
    private val remoteDataSource: ApiService,
    private val localDataSource: AppDao
) {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun setMutasi(
        userId: String,
        mutasiId: String,
        isApprove: String
    ): MutableLiveData<ApiResponse<ResponseBody>> {
        val responseData = MutableLiveData<ApiResponse<ResponseBody>>()

        val params = HashMap<String, String>()
        params["supervisor_id"] = userId
        params["mutasi_id"] = mutasiId
        params["isApprove"] = isApprove
        Log.e("aim", "set mutasi body : $params")

        remoteDataSource.setMutasi(params).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

}