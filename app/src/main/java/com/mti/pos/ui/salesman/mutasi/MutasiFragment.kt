package com.mti.pos.ui.salesman.mutasi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.mti.pos.R
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentMutasiBinding
import com.mti.pos.ui.salesman.adapter.MutasiAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MutasiFragment : Fragment(), MutasiAdapter.MutasiItemListener {

    private lateinit var binding: FragmentMutasiBinding
    private lateinit var adapter: MutasiAdapter
    private val viewModel: MutasiViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMutasiBinding.inflate(inflater, container, false)
        adapter = MutasiAdapter(this)
        binding.recyclerMutasi.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        buttonListener()
    }

    override fun onItemClick(item: MutasiListRequest.Data) {
        val bundle = Bundle()
        bundle.putString("data", Gson().toJson(item))
        findNavController().navigate(R.id.nav_mutasi_detail, bundle)
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.refreshLayout.setOnRefreshListener {
            setupObserver()
        }

        binding.btnAddMutasi.setOnClickListener {
            findNavController().navigate(R.id.nav_mutasi_add)
        }
    }

    private fun setupObserver() {
        viewModel.getProfile().observe(viewLifecycleOwner, { profile ->
            getMutasi(
                userId = profile.first().userId.toString()
            )
        })
    }

    private fun getMutasi(userId: String) {
        binding.refreshLayout.isRefreshing = true

        viewModel.getMutasi(userId).observe(viewLifecycleOwner, {
            binding.refreshLayout.isRefreshing = false
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    binding.hasCustomer = !it.response?.data.isNullOrEmpty()
                    adapter.submitList(it.response?.data)
                }
                ApiResponse.ApiStatus.ERROR -> {
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

}