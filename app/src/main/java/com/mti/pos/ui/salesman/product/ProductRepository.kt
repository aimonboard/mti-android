package com.mti.pos.ui.salesman.product

import android.util.Log
import androidx.lifecycle.LiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.model.ProductStockModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepository @Inject constructor(
    private val localDataSource: AppDao
) {

    fun getProduct(searchText: String): LiveData<List<ProductStockModel>> {
        Log.e("aim", "search text : $searchText")
        return localDataSource.productStock(searchText)
    }

}