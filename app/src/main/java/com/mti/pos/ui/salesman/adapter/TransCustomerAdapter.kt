package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.databinding.ItemTransactionCustomerBinding

class TransCustomerAdapter :
    ListAdapter<CustomerEntity, TransCustomerAdapter.ViewHolder>(CustomerDiffUtil()) {

    private var listener: CustomerItemListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_transaction_customer,
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    fun setListener(listener: CustomerItemListener) {
        try {
            this.listener = listener
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class ViewHolder(private val itemBinding: ItemTransactionCustomerBinding, private val listener: CustomerItemListener?) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: CustomerEntity) {
            itemBinding.codeValue.text = item.customerCode
            itemBinding.nameValue.text = item.customerName
            itemBinding.addressValue.text = item.customerAddress

            itemBinding.root.setOnClickListener {
                listener?.onCustomerClick(item)
            }
        }

    }

    private class CustomerDiffUtil : DiffUtil.ItemCallback<CustomerEntity>() {
        override fun areItemsTheSame(oldItem: CustomerEntity, newItem: CustomerEntity): Boolean {
            return oldItem.customerId == newItem.customerId
        }

        override fun areContentsTheSame(oldItem: CustomerEntity, newItem: CustomerEntity): Boolean {
            return oldItem == newItem
        }

    }

    interface CustomerItemListener {
        fun onCustomerClick(customerData: CustomerEntity)
    }

}