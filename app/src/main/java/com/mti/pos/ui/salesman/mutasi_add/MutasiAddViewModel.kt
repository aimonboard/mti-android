package com.mti.pos.ui.salesman.mutasi_add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.model.MutasiModel
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.ResponseBody
import javax.inject.Inject

@HiltViewModel
class MutasiAddViewModel @Inject constructor(
    private val repository: MutasiAddRepository,
) : ViewModel() {

    var productLive = MutableLiveData<ArrayList<ProductQtyModel>>()
    private val productData = ArrayList<ProductQtyModel>()

    var userId = 0
    var warehouseCode = ""

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun getProduct(): LiveData<List<ProductQtyModel>> {
        return repository.getProduct()
    }

    fun addProduct(products: ProductQtyModel) {
        if (productData.isNotEmpty()) {
            var duplicate = false
            var i = 0
            productData.forEachIndexed { index, it ->
                if (it.productId == products.productId) {
                    duplicate = true
                    i = index
                }
            }
            if (duplicate) productData[i].productQty = products.productQty
            else productData.add(products)
        } else {
            productData.add(products)
        }
        productLive.value = productData
    }

    fun removeProduct(products: ProductQtyModel) {
        productData.remove(products)
        productLive.value = productData
    }

    fun sendMutasi(): LiveData<ApiResponse<ResponseBody>> {
        val mutasiData = MutasiModel(
            salesmanId = userId,
            warehouseCode = warehouseCode,
            productData = productData
        )
        return repository.sendTransaction(mutasiData)
    }

}