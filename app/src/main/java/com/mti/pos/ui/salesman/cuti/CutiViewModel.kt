package com.mti.pos.ui.salesman.cuti

import androidx.lifecycle.*
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.CutiRequest
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class CutiViewModel @Inject constructor(
    private val repository: CutiRepository
) : ViewModel() {

//    fun getProfile(): LiveData<List<ProfileEntity>> {
//        return repository.getProfile()
//    }
//
//    fun getCuti(): LiveData<ApiResponse<CutiRequest>> {
//        val userEntity: MutableLiveData<List<ProfileEntity>> = repository.getProfile()
//        val cutiResponse = MutableLiveData<ApiResponse<CutiRequest>>()
//
//        userEntity.mutation {
//            Log.e("aim", "COK : $it")
////            cutiResponse.value = repository.getCuti(it.value!!.first().userId).value
//        }
//        return cutiResponse
//    }




    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun getCuti(userId: Int): LiveData<ApiResponse<CutiRequest>> {
        return repository.getCuti(userId)
    }

    fun <T> MutableLiveData<T>.mutation(actions: (MutableLiveData<T>) -> Unit) {
        actions(this)
        this.value = this.value
    }




}