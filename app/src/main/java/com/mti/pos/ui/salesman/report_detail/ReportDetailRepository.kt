package com.mti.pos.ui.salesman.report_detail

import android.bluetooth.BluetoothAdapter
import androidx.lifecycle.LiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.TransactionDetailEntity
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.data.model.DeviceModel
import com.mti.pos.util.BluetoothUtil
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReportDetailRepository @Inject constructor(
    private val localDataSource: AppDao,
    private val btAdapter: BluetoothAdapter,
    private val btUtil: BluetoothUtil
) {

    fun getTransactionHeader(transactionId: Int): LiveData<List<TransactionEntity>> {
        return localDataSource.transactionGetById(transactionId)
    }

    fun getTransactionDetail(transactionId: Int): LiveData<List<TransactionDetailEntity>> {
        return localDataSource.transactionDetailGetByTransId(transactionId)
    }

    fun getPairedDevice(): List<DeviceModel> {
        val pairedData = ArrayList<DeviceModel>()
        btAdapter.bondedDevices.forEach{
            pairedData.add(
                DeviceModel(
                    deviceName = it.name,
                    deviceAddress = it.address,
                )
            )
        }
        return pairedData
    }

    fun printTransaction(
        transactionHeader: TransactionEntity,
        transactionDetail: List<TransactionDetailEntity>
    ) {
        btUtil.printTransaction(
            transactionHeader,
            transactionDetail
        )
    }

}