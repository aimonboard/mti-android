package com.mti.pos.ui.salesman.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.CutiRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.ui.salesman.cuti.CutiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ActivityViewModel @Inject constructor(
    private val repository: CutiRepository
) : ViewModel() {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun getCuti(userId: Int): LiveData<ApiResponse<CutiRequest>> {
        return repository.getCuti(userId)
    }

}