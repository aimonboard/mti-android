package com.mti.pos.ui.salesman.cuti_form

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mti.pos.R
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentCutiFormBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_success.view.*
import java.util.*

@AndroidEntryPoint
class CutiFormFragment : Fragment() {

    private lateinit var binding: FragmentCutiFormBinding
    private val viewModel: CutiFormViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCutiFormBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        generateSpinner()
        setupObserver()
        buttonListener()
    }

    private fun buttonListener() {
        binding.btnBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.startValue.setOnClickListener {
            val c = Calendar.getInstance()
            val yearCalendar = c.get(Calendar.YEAR)
            val monthCalendar = c.get(Calendar.MONTH)
            val dayCalendar = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(requireContext(), { view, year, monthOfYear, dayOfMonth ->
                val mCalendar = Calendar.getInstance()
                mCalendar.set(year, monthOfYear, dayOfMonth)

                viewModel.dateChoose = mCalendar.time
                viewModel.selectedDate()

                viewModel.durationInt = 0
                viewModel.duration.value = "1"
            }, yearCalendar, monthCalendar, dayCalendar)

            dpd.show()
        }

        binding.btnPlus.setOnClickListener {
            viewModel.durationPlus()
        }

        binding.btnMinus.setOnClickListener {
            viewModel.durationMinus()
        }

        binding.btnCuti.setOnClickListener {
            if (binding.startValue.text.isNotEmpty()) {
                sendCuti()
            } else {
                Toast.makeText(requireContext(), "Tanngal cuti harus diisi", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun generateSpinner() {
        val typeData = arrayOf(
            "Tahunan",
            "Melahirkan",
            "Khusus",
            "Lain-lain"
        )
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner, typeData)
        binding.spinnerType.adapter = arrayAdapter

        binding.spinnerType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.selectedSpinner(position)
            }
        }
    }

    private fun setupObserver() {
        viewModel.getProfile().observe(viewLifecycleOwner, { profile ->
            viewModel.userId.value = profile.first().userId.toString()
            viewModel.userName.value = profile.first().userName
        })

        viewModel.dateError.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun sendCuti() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.sendCuti().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    progressDialog.dismiss()
                    successDialog()
                }
                ApiResponse.ApiStatus.ERROR -> {
                    progressDialog.dismiss()
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun successDialog() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.dialog_success, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)

        dialogView.message.text = resources.getString(R.string.dialog_success)
        dialogView.btn_ok.setOnClickListener {
            alertDialog.dismiss()
            requireActivity().onBackPressed()
        }

        alertDialog.show()
    }

}