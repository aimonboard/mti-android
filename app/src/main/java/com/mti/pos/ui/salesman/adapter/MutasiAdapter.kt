package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.databinding.ItemMutasiBinding
import com.mti.pos.util.EnumUtil

class MutasiAdapter(private val listener: MutasiItemListener) :
    ListAdapter<MutasiListRequest.Data, MutasiAdapter.ViewHolder>(ProductDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_mutasi,
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    class ViewHolder(
        private val itemBinding: ItemMutasiBinding,
        private val listener: MutasiItemListener
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: MutasiListRequest.Data) {
            when (item.status) {
                EnumUtil.MutasiStatus.Pending.code.toString() -> {
                    itemBinding.statusValue.text = "Pengajuan"
                    itemBinding.statusValue.setBackgroundResource(R.drawable.shape_button_orange)
                }
                EnumUtil.MutasiStatus.Approve.code.toString() -> {
                    itemBinding.statusValue.text = "Diterima"
                    itemBinding.statusValue.setBackgroundResource(R.drawable.shape_button_green)
                }
                EnumUtil.MutasiStatus.Reject.code.toString() -> {
                    itemBinding.statusValue.text = "Ditolak"
                    itemBinding.statusValue.setBackgroundResource(R.drawable.shape_button_red)
                }
            }

            itemBinding.numberValue.text = item.nomorMutasi ?: "-"
            itemBinding.requestDateValue.text = item.date ?: "-"
            itemBinding.requestByValue.text = item.requestBy?.first()?.staffName ?: "-"

            itemBinding.root.setOnClickListener {
                listener.onItemClick(item)
            }
        }

    }

    private class ProductDiffUtil : DiffUtil.ItemCallback<MutasiListRequest.Data>() {
        override fun areItemsTheSame(
            oldItem: MutasiListRequest.Data,
            newItem: MutasiListRequest.Data
        ): Boolean {
            return oldItem.mutasiId == newItem.mutasiId
        }

        override fun areContentsTheSame(
            oldItem: MutasiListRequest.Data,
            newItem: MutasiListRequest.Data
        ): Boolean {
            return oldItem == newItem
        }

    }

    interface MutasiItemListener {
        fun onItemClick(item: MutasiListRequest.Data)
    }

}