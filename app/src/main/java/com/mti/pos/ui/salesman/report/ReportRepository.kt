package com.mti.pos.ui.salesman.report

import androidx.lifecycle.LiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.TransactionEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReportRepository @Inject constructor(
    val localDataSource: AppDao
) {

    fun getHeaderTransaction(startDate: String, endDate: String): LiveData<List<TransactionEntity>> {
        return localDataSource.transactionGetAll(startDate, endDate)
    }

}