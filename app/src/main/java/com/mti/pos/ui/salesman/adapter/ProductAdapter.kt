package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.model.ProductStockModel
import com.mti.pos.databinding.ItemProductBinding

class ProductAdapter: ListAdapter<ProductStockModel, ProductAdapter.ViewHolder>(ProductDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_product,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    class ViewHolder(private val itemBinding: ItemProductBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(item: ProductStockModel) {
            itemBinding.productCode.text = item.productCode
            itemBinding.productName.text = item.productName
            itemBinding.productStock.text = "${item.stockProductQty} Packs"
        }

    }

    private class ProductDiffUtil : DiffUtil.ItemCallback<ProductStockModel>() {
        override fun areItemsTheSame(oldItem: ProductStockModel, newItem: ProductStockModel): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: ProductStockModel, newItem: ProductStockModel): Boolean {
            return oldItem == newItem
        }

    }

}