package com.mti.pos.ui.supervisor

import android.content.*
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.mti.pos.R
import com.mti.pos.util.BluetoothUtil
import com.mti.pos.util.GpsUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_menu_supervisor.*
import javax.inject.Inject

@AndroidEntryPoint
class MenuSupervisorActivity : AppCompatActivity() {

    companion object {
        var lat = ""
        var lon = ""
    }

    @Inject
    lateinit var bluetoothUtil: BluetoothUtil

    @Inject
    lateinit var gpsUtil: GpsUtil

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_supervisor)
        setupNavigation()
    }

//    override fun onBackPressed() {
//        if(!drawer_layout.isDrawerOpen(GravityCompat.START)) drawer_layout.openDrawer(GravityCompat.START)
//        else drawer_layout.closeDrawer(GravityCompat.START)
//    }

    override fun onResume() {
        super.onResume()
        bluetoothUtil.enableBluetooth(true)
    }

    override fun onPause() {
        super.onPause()
        bluetoothUtil.enableBluetooth(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothUtil.enableBluetooth(false)
    }

    private fun setupNavigation() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        nav_view.setupWithNavController(navController)

        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> {
                    navController.navigate(R.id.nav_home, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                R.id.menu_profile -> {
                    navController.navigate(R.id.nav_profile, null)
                    drawer_layout?.closeDrawer(GravityCompat.START)
                    return@setNavigationItemSelectedListener true
                }
                else -> {
                    return@setNavigationItemSelectedListener false
                }
            }
        }
    }

}