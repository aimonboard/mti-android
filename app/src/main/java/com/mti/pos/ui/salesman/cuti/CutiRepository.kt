package com.mti.pos.ui.salesman.cuti

import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.CutiRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CutiRepository @Inject constructor(
    val remoteDataSource: ApiService,
    val localDataSource: AppDao
) {

    fun getProfile(): MutableLiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun getCuti(userId: Int): MutableLiveData<ApiResponse<CutiRequest>> {
        val responseData = MutableLiveData<ApiResponse<CutiRequest>>()

        val params = HashMap<String, String>()
        params["salesman_id"] = userId.toString()
        params["year"] = "2021"

        remoteDataSource.getCuti(params).enqueue(object : Callback<CutiRequest> {
            override fun onFailure(call: Call<CutiRequest>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(call: Call<CutiRequest>, response: Response<CutiRequest>) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

}