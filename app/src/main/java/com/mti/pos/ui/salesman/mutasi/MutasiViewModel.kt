package com.mti.pos.ui.salesman.mutasi

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MutasiViewModel @Inject constructor(
    private val repository: MutasiRepository
) : ViewModel() {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun getMutasi(userId: String): LiveData<ApiResponse<MutasiListRequest>> {
        return repository.getMutasi(userId)
    }

}