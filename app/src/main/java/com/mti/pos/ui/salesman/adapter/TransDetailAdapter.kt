package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.database.entity.TransactionDetailEntity
import com.mti.pos.databinding.ItemTransactionDetailBinding
import com.mti.pos.util.Formater

class TransDetailAdapter :
    ListAdapter<TransactionDetailEntity, TransDetailAdapter.ViewHolder>(TransDetailDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_transaction_detail,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(position, itemCount, getItem(position))

    class ViewHolder(private val itemBinding: ItemTransactionDetailBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, itemCount: Int, item: TransactionDetailEntity) {
            val priceString = "Rp ${Formater().idrFormat(item.transDetailPrice)}"
            val qtyString = "(${item.transDetailQtyPCS})"
            val totalString = "Rp ${Formater().idrFormat(item.transDetailSubTotal)}"

            itemBinding.nameValue.text = item.transDetail_productName
            itemBinding.priceValue.text = "$priceString $qtyString"
            itemBinding.totalValue.text = totalString

            if (position + 1 == itemCount) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }
        }

    }

    private class TransDetailDiffUtil : DiffUtil.ItemCallback<TransactionDetailEntity>() {
        override fun areItemsTheSame(
            oldItem: TransactionDetailEntity,
            newItem: TransactionDetailEntity
        ): Boolean {
            return oldItem.transDetailId == newItem.transDetailId
        }

        override fun areContentsTheSame(
            oldItem: TransactionDetailEntity,
            newItem: TransactionDetailEntity
        ): Boolean {
            return oldItem == newItem
        }

    }

}