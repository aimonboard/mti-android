package com.mti.pos.ui.salesman.transaction

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.model.ProductStockPriceModel
import com.mti.pos.data.model.TransactionModel
import com.mti.pos.ui.salesman.MenuSalesmanActivity
import com.mti.pos.util.BluetoothUtil
import com.mti.pos.util.Formater
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val repository: TransactionRepository,
    private val bluetoothUtil: BluetoothUtil
) : ViewModel() {

    val isDiscountErr = MutableLiveData(false)
    val isPaymentErr = MutableLiveData(false)
    val isButtonEnable = MutableLiveData(false)

    val customerNameBinding = MutableLiveData("")
    val discountBinding = MutableLiveData("")
    val totalBinding = MutableLiveData("")
    val cashBackBinding = MutableLiveData("")
    val payBinding = MutableLiveData("")

    var productLive = MutableLiveData<ArrayList<ProductStockPriceModel>>()
    private val productData = ArrayList<ProductStockPriceModel>()

    var customerId = 0
    var customerName = ""
    var customerType = 0
    var userId = 0
    var userName = ""
    var warehouseCode = ""
    var transactionType = 0

    private var transactionCode = ""
    private var total = 0.0
    private var discount = 0.0
    private var grandTotal = 0.0
    private var payment = 0.0
    private var cashBack = 0.0


    fun enableBluetooth(turnOn: Boolean) {
        bluetoothUtil.enableBluetooth(turnOn)
    }

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun getCustomer(searchText: String): LiveData<List<CustomerEntity>> {
        return repository.getCustomer(searchText)
    }

    fun getCustomerById(customerId: Int): LiveData<List<CustomerEntity>> {
        return repository.getCustomerById(customerId)
    }

    fun getProduct(searchText: String): LiveData<List<ProductStockPriceModel>> {
        return repository.getProductStockPrice(customerType, searchText)
    }

    fun addProduct(products: ProductStockPriceModel) {
        if (productData.isNotEmpty()) {
            var duplicate = false
            var i = 0
            productData.forEachIndexed { index, it ->
                if (it.productId == products.productId && it.priceListId == products.priceListId) {
                    duplicate = true
                    i = index
                }
            }
            if (duplicate) productData[i].stockProductQty = products.stockProductQty
            else productData.add(products)
        } else {
            productData.add(products)
        }
        calculateProduct()
        productLive.value = productData
    }

    fun removeProduct(products: ProductStockPriceModel) {
        productData.remove(products)
        calculateProduct()
        productLive.value = productData
    }

    fun clearProduct() {
        productData.clear()
        calculateProduct()
        productLive.value = productData
    }

    private fun calculateProduct() {
        total = 0.0
        discount = 0.0
        grandTotal = 0.0
        payment = 0.0
        cashBack = 0.0
        discountBinding.value = ""
        cashBackBinding.value = ""
        payBinding.value = ""
        isDiscountErr.value = false
        isPaymentErr.value = false
        enableButton()

        if (productData.isNotEmpty()) {
            productData.forEach {
                total += it.priceListPrice!! * it.stockProductQty!!.toDouble()
            }
            totalBinding.value = Formater().idrFormat(total)
        } else {
            totalBinding.value = ""
        }
    }

    fun discountTextWatcher(discountValue: Double) {
        cashBackBinding.value = ""
        payBinding.value = ""
        enableButton()

        if (discountValue <= total) {
            discount = discountValue
            grandTotal = total - discountValue
            isDiscountErr.value = false
        } else {
            discount = 0.0
            grandTotal = total
            isDiscountErr.value = true
        }
        totalBinding.value = Formater().idrFormat(grandTotal)
    }

    fun paymentTextWatcher(paymentValue: Double) {
        enableButton()

        if (discount == 0.0) {
            grandTotal = total
        }

        payment = paymentValue
        cashBack = paymentValue - grandTotal

        if (cashBack >= 0) {
            isPaymentErr.value = false
            cashBackBinding.value = Formater().idrFormat(cashBack)
        } else {
            isPaymentErr.value = true
            cashBackBinding.value = ""
        }
    }

    fun sendTransaction() {
        val transactionData = TransactionModel(
            type = transactionType,
            code = transactionCode,
            date = transactionDate(),
            customerId = customerId,
            customerName = customerNameBinding.value ?: "",
            salesId = userId,
            salesName = userName,
            total = total,
            discount = discount,
            grandTotal = grandTotal,
            payment = payment,
            cashBack = cashBack,
            latitude = MenuSalesmanActivity.lat,
            longitude = MenuSalesmanActivity.lon,
            productData = productData
        )

        repository.sendTransaction(transactionData)
    }

    fun getTransactionId(): LiveData<Int> {
        return repository.transactionId
    }

    private fun enableButton() {
        isButtonEnable.value = cashBackBinding.value!!.isNotEmpty() && productData.isNotEmpty()
    }

    fun incrementTransactionCode(currentCode: Int): Int {
        val transCodeFormat = String.format("%03d", currentCode)
        val transDateFormat = SimpleDateFormat("yyMMdd", Locale.getDefault())
        val dateCode = transDateFormat.format(Date())

        // GC01/210502/003
        transactionCode = "$warehouseCode/$dateCode/$transCodeFormat"
        Log.e("aim", "transaction code : $transactionCode")

        // set next transaction code
        return if (currentCode == 999) 1 else currentCode + 1
    }

    private fun transactionDate(): String {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        return format.format(Date())
    }

    fun clearAllData() {
        isDiscountErr.value = false
        isPaymentErr.value = false
        isButtonEnable.value = false

        customerNameBinding.value = ""
        discountBinding.value = ""
        totalBinding.value = ""
        cashBackBinding.value = ""
        payBinding.value = ""

        customerId = 0
        customerName = ""
        customerType = 0
        userId = 0
        userName = ""

        productData.clear()
        productLive.value = productData

        total = 0.0
        discount = 0.0
        grandTotal = 0.0
        payment = 0.0
        cashBack = 0.0
    }

}