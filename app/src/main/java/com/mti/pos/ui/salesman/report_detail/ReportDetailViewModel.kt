package com.mti.pos.ui.salesman.report_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.TransactionDetailEntity
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.data.model.DeviceModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ReportDetailViewModel @Inject constructor(
    private val repository: ReportDetailRepository
): ViewModel() {

    val kodeBinding = MutableLiveData("")
    val dateBinding = MutableLiveData("")
    val customerBinding = MutableLiveData("")
    val totalBinding = MutableLiveData("")
    val paymentBinding = MutableLiveData("")
    val cashBackBinding = MutableLiveData("")

    lateinit var transactionHeader: TransactionEntity
    lateinit var transactionDetail: List<TransactionDetailEntity>

    fun getTransactionHeader(transactionId: Int): LiveData<List<TransactionEntity>> {
        return repository.getTransactionHeader(transactionId)
    }

    fun getTransactionDetail(transactionId: Int): LiveData<List<TransactionDetailEntity>> {
        return repository.getTransactionDetail(transactionId)
    }

    fun getPairedDevice(): List<DeviceModel> {
        return repository.getPairedDevice()
    }

    fun printTransaction() {
        repository.printTransaction(
            transactionHeader,
            transactionDetail
        )
    }

}