package com.mti.pos.ui.supervisor.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mti.pos.R
import com.mti.pos.databinding.FragmentHomeSupervisorBinding

class HomeSupervisorFragment : Fragment() {

    private lateinit var binding: FragmentHomeSupervisorBinding

    //    private lateinit var adapter: ReportAdapter
    private val viewModel: HomeSupervisorViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeSupervisorBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
//        adapter = ReportAdapter(this)
//        binding.recyclerProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        setupObservers()
        buttonListener()
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }
    }

}