package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.data.model.ProductStockPriceModel
import com.mti.pos.databinding.ItemTransactionProductViewBinding
import com.mti.pos.util.Formater
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class TransProductViewAdapter (private val listener: ProductItemListener) : RecyclerView.Adapter<TransProductViewAdapter.ViewModel>() {

    private var items = ArrayList<ProductStockPriceModel>()

    fun setItems(items: ArrayList<ProductStockPriceModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getAllItems() = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val binding = ItemTransactionProductViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewModel(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) = holder.bind(position, items[position])

    inner class ViewModel(private val itemBinding: ItemTransactionProductViewBinding, private val listener: ProductItemListener) : RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, item: ProductStockPriceModel) {
            itemBinding.productCode.text = item.productCode
            itemBinding.productName.text = item.productName

            val priceString = "Rp ${Formater().idrFormat(item.priceListPrice ?: 0.0)} / Pack"
            val qtyString = "(${item.stockProductQty})"
            itemBinding.productPrice.text = "$priceString $qtyString"

            val total = item.priceListPrice!! * item.stockProductQty!!.toDouble()
            val totalString = "Rp ${Formater().idrFormat(total)}"
            itemBinding.productTotal.text = totalString

            if (position == items.size-1) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }

            itemBinding.btnDelete.setOnClickListener {
                items.remove(item)
                listener.onProductDelete(item)
            }

        }
    }

    interface ProductItemListener {
        fun onProductDelete(product: ProductStockPriceModel)
    }

}