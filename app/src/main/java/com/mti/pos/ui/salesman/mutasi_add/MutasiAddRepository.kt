package com.mti.pos.ui.salesman.mutasi_add

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.model.MutasiModel
import com.mti.pos.data.model.ProductQtyModel
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MutasiAddRepository @Inject constructor(
    private val remoteDataSource: ApiService,
    private val localDataSource: AppDao
) {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun getProduct(): LiveData<List<ProductQtyModel>> {
        return localDataSource.productQty()
    }

    fun sendTransaction(productData: MutasiModel): MutableLiveData<ApiResponse<ResponseBody>> {
        val responseData = MutableLiveData<ApiResponse<ResponseBody>>()

        remoteDataSource.sendMutasi(param(productData)).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

    private fun param(mutasiModel: MutasiModel): RequestBody {
        val bodyArray = JSONArray()

        val mainObject = JSONObject()
        mainObject.put("salesman_id", mutasiModel.salesmanId)
        mainObject.put("warehouse_code", mutasiModel.warehouseCode)

        val detailArray = JSONArray()
        mutasiModel.productData.forEach {
            val detailObject = JSONObject()
            detailObject.put("item_id", it.productId)
            detailObject.put("qty", it.productQty)
            detailArray.put(detailObject)
        }

        mainObject.put("product", detailArray)
        bodyArray.put(mainObject)
        Log.e("aim", "body : $bodyArray")

        return RequestBody.create(
            MediaType.parse("application/json; charset=utf-8"),
            bodyArray.toString()
        )
    }

}