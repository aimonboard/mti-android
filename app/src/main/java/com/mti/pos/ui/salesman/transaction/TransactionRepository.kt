package com.mti.pos.ui.salesman.transaction

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.database.entity.TransactionDetailEntity
import com.mti.pos.data.database.entity.TransactionEntity
import com.mti.pos.data.model.ProductStockPriceModel
import com.mti.pos.data.model.TransactionModel
import com.mti.pos.data.remote.TransactionRequest
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TransactionRepository @Inject constructor(
    private val remoteDataSource: ApiService,
    private val localDataSource: AppDao
) {

    val transactionId = MutableLiveData<Int>()

    fun getProfile(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun getCustomer(searchText: String): LiveData<List<CustomerEntity>> {
        return localDataSource.customerGetAll(searchText)
    }

    fun getCustomerById(customerId: Int): LiveData<List<CustomerEntity>> {
        return localDataSource.customerGetById(customerId)
    }

    fun getProductStockPrice(
        customerType: Int,
        searchText: String
    ): LiveData<List<ProductStockPriceModel>> {
        return localDataSource.productStockPrice(customerType, searchText)
    }

    fun sendTransaction(transactionData: TransactionModel) {
        remoteDataSource.transaction(generateBody(transactionData))
            .enqueue(object : Callback<TransactionRequest> {
                override fun onFailure(call: Call<TransactionRequest>, t: Throwable) {
                    Log.e("aim", "err : ${t.message}")
                    insertTransaction(status = false, transactionData = transactionData)
                }

                override fun onResponse(
                    call: Call<TransactionRequest>,
                    response: Response<TransactionRequest>
                ) {
                    Log.e("aim", "body : ${response.body()}, message : ${response.message()}")
                    insertTransaction(status = true, transactionData = transactionData)
                }
            })
    }

    private fun generateBody(transactionData: TransactionModel): RequestBody {
        val bodyArray = JSONArray()

        val mainObject = JSONObject()
        mainObject.put("invoice_type", transactionData.type)
        mainObject.put("invoice_code", transactionData.code)
        mainObject.put("invoice_date", transactionData.date)
        mainObject.put("salesman_id", transactionData.salesId)
        mainObject.put("customer_id", transactionData.customerId)
        mainObject.put("latitude", transactionData.latitude)
        mainObject.put("longitude", transactionData.longitude)

        val detailArray = JSONArray()
        transactionData.productData.forEach {
            val detailObject = JSONObject()
            detailObject.put("item_id", it.productId)
            detailObject.put("item_name", it.productName)
            detailObject.put("qty", it.stockProductQty)
            detailObject.put("measurement", "Pack")
            detailObject.put("unit_price", it.priceListPrice)
            detailArray.put(detailObject)
        }

        mainObject.put("detail", detailArray)
        bodyArray.put(mainObject)
        Log.e("aim", "body : $bodyArray")

        return RequestBody.create(
            MediaType.parse("application/json; charset=utf-8"),
            bodyArray.toString()
        )
    }

    private fun insertTransaction(status: Boolean, transactionData: TransactionModel) {

        GlobalScope.launch {
            val mTransactionId = localDataSource.transactionInsert(
                TransactionEntity(
                    transId = 0,
                    transType = transactionData.type,
                    transCode = transactionData.code,
                    transDate = transactionData.date,
                    trans_customerId = transactionData.customerId,
                    trans_customerName = transactionData.customerName,
                    trans_salesId = transactionData.salesId,
                    trans_salesName = transactionData.salesName,
                    transTotal = transactionData.total,
                    transDiscount = transactionData.discount,
                    transGrandTotal = transactionData.grandTotal,
                    transPayment = transactionData.payment,
                    transCashBack = transactionData.cashBack,
                    transLatitude = transactionData.latitude,
                    transLongitude = transactionData.longitude,
                    transIsUpload = status
                )
            )
            transactionId.postValue(mTransactionId.toInt())

            transactionData.productData.forEach {
                localDataSource.transactionDetailInsert(
                    TransactionDetailEntity(
                        transDetailId = 0,
                        transDetail_transId = mTransactionId.toInt(),
                        transDetail_productId = it.productId,
                        transDetail_productName = it.productName,
                        transDetailMeasurement = "",
                        transDetailQtyUOM = 0,
                        transDetailConversion = 0,
                        transDetailQtyPCS = it.stockProductQty!!,
                        transDetailPrice = it.priceListPrice!!,
                        transDetailDiscount = 0.0,
                        transDetailSubTotal = it.priceListPrice!! * it.stockProductQty!!.toDouble(),
                        transDetailDate = transactionData.date
                    )
                )

                // update stok
                localDataSource.stockUpdate(it.productId, it.stockProductQty!!)

            }
        }
    }

}