package com.mti.pos.ui.salesman.customer_add

import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.mti.pos.databinding.FragmentCustomerAddMapsBinding
import com.mti.pos.ui.salesman.MenuSalesmanActivity
import com.mti.pos.util.GpsUtil
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class CustomerAddMapsFragment : Fragment(), OnMapReadyCallback, LocationListener {

    @Inject
    lateinit var gpsUtil: GpsUtil

    private lateinit var binding: FragmentCustomerAddMapsBinding
    private lateinit var coordinate: LatLng

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCustomerAddMapsBinding.inflate(inflater, container, false)
        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonListener()
    }

    override fun onResume() {
        binding.mapView.onResume()
        super.onResume()
    }

    override fun onDestroyView() {
        binding.mapView.onDestroy()
        super.onDestroyView()
    }

    override fun onLocationChanged(location: Location) {}

    override fun onMapReady(googleMap: GoogleMap) {
        MapsInitializer.initialize(requireContext())

        val customerLat = arguments?.getString("customer_lat") ?: ""
        val customerLon = arguments?.getString("customer_lon") ?: ""
        coordinate = if (customerLat.isNotEmpty() && customerLon.isNotEmpty()) {
            LatLng(
                customerLat.toDouble(),
                customerLon.toDouble()
            )
        } else {
            LatLng(
                MenuSalesmanActivity.lat.toDouble(),
                MenuSalesmanActivity.lon.toDouble()
            )
        }
        val currentLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15f)
        googleMap.animateCamera(currentLocation)

        googleMap.setOnCameraIdleListener {
            coordinate = googleMap.cameraPosition.target
            var addressChosen = ""
            try {
                addressChosen = gpsUtil.getAddressFromLatLong(
                    context = requireContext(),
                    latitude = coordinate.latitude,
                    longitude = coordinate.longitude
                )
            } catch (e: Exception) {
                e.stackTrace
            }
            binding.addressValue.text = addressChosen
        }

        binding.btnMyLocation.setOnClickListener {
            coordinate = LatLng(
                MenuSalesmanActivity.lat.toDouble(),
                MenuSalesmanActivity.lon.toDouble()
            )

            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(coordinate, 15f)
            )
        }

    }

    private fun buttonListener() {
        binding.btnChoose.setOnClickListener {
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                "address",
                "${binding.addressValue.text}|0|${coordinate.latitude}|0|${coordinate.longitude}"
            )
            findNavController().popBackStack()
        }
    }

}