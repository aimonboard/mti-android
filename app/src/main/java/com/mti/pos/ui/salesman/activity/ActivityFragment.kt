package com.mti.pos.ui.salesman.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.mti.pos.R
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentActivityBinding
import com.mti.pos.ui.salesman.adapter.CutiAdapter
import com.mti.pos.util.GpsUtil
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class ActivityFragment : Fragment() {

    @Inject
    lateinit var gpsUtil: GpsUtil

    private lateinit var binding: FragmentActivityBinding
    private lateinit var adapter: CutiAdapter
    private val viewModel: ActivityViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentActivityBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        adapter = CutiAdapter()
        binding.recyclerActivity.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObserver()
        buttonListener()
    }

    private fun buttonListener() {
        binding.refreshLayout.setOnRefreshListener {
            setupObserver()
        }

        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnRoute.setOnClickListener {
            testRoute()
        }
    }

    private fun setupObserver() {
        binding.refreshLayout.isRefreshing = true

        viewModel.getProfile().observe(viewLifecycleOwner, { profile ->

            viewModel.getCuti(profile.first().userId).observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    ApiResponse.ApiStatus.SUCCESS -> {
                        binding.refreshLayout.isRefreshing = false

                        binding.hasCustomer = !it.response?.data.isNullOrEmpty()
                        adapter.submitList(it.response?.data)
                    }
                    ApiResponse.ApiStatus.ERROR -> {
                        binding.refreshLayout.isRefreshing = false

                        Toast.makeText(
                            requireContext(),
                            it.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        })

    }

    private fun testRoute() {
        val dummyLocation = gpsUtil.sortLocations()

        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(gpsUtil.generateRouteUrl(dummyLocation))
        )
        startActivity(intent)
    }

}