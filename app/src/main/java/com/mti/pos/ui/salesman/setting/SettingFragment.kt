package com.mti.pos.ui.salesman.setting

import android.bluetooth.BluetoothDevice.ACTION_FOUND
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.mti.pos.R
import com.mti.pos.data.model.DeviceModel
import com.mti.pos.databinding.FragmentSettingBinding
import com.mti.pos.ui.salesman.adapter.BluetoothNewAdapter
import com.mti.pos.ui.salesman.adapter.BluetoothPairedAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingFragment : Fragment(), BluetoothNewAdapter.NewDeviceListener {

    private lateinit var binding: FragmentSettingBinding
    private lateinit var pairDeviceAdapter: BluetoothPairedAdapter
    private lateinit var newDeviceAdapter: BluetoothNewAdapter
    private val viewModel: SettingViewModel by viewModels()

    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val action = intent.action
            if (action == ACTION_FOUND) {
                viewModel.scanBluetoothResult(intent)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        pairDeviceAdapter = BluetoothPairedAdapter()
        binding.recyclerPair.adapter = pairDeviceAdapter
        newDeviceAdapter = BluetoothNewAdapter(this)
        binding.recyclerNew.adapter = newDeviceAdapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonListener()
        setupObserver()
        viewModel.getPairDevice()
    }

//    override fun onStop() {
//        super.onStop()
//        requireActivity().unregisterReceiver(mReceiver)
//        viewModel.stopScanBluetooth()
//    }

    override fun onPairDevice(device: DeviceModel) {
        viewModel.pairNewBluetooth(device)
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnRefresh.setOnClickListener {
            binding.loadingPair.visibility = View.VISIBLE
            viewModel.getPairDevice()

            val intentFoundBluetooth = IntentFilter(ACTION_FOUND)
            requireActivity().registerReceiver(mReceiver, intentFoundBluetooth)
            viewModel.startScanBluetooth()
        }

        binding.btnAddDevice.setOnClickListener {
            binding.loadingScan.visibility = View.VISIBLE
            viewModel.getPairDevice()

            val intentFoundBluetooth = IntentFilter(ACTION_FOUND)
            requireActivity().registerReceiver(mReceiver, intentFoundBluetooth)
            viewModel.startScanBluetooth()
        }
    }

    private fun setupObserver() {
        viewModel.pairedDevice.observe(viewLifecycleOwner, Observer {
            binding.loadingPair.visibility = View.GONE
            binding.hasPairDevice = !it.isNullOrEmpty()
            pairDeviceAdapter.setItems(it)
        })

        viewModel.newDevice.observe(viewLifecycleOwner, Observer {
            binding.loadingScan.visibility = View.GONE
            binding.hasNewDevice = newDeviceAdapter.itemCount > 0
            newDeviceAdapter.addItem(it)
        })
    }



}