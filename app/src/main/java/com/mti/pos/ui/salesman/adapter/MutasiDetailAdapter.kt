package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.R
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.databinding.ItemMutasiDetailBinding

class MutasiDetailAdapter :
    ListAdapter<MutasiListRequest.Data.Product, MutasiDetailAdapter.ViewHolder>(MutasiDetailDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_mutasi_detail,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(position, itemCount, getItem(position))

    class ViewHolder(private val itemBinding: ItemMutasiDetailBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, itemCount: Int, item: MutasiListRequest.Data.Product) {
            val stringTotal = "${item.qty} Packs"
            itemBinding.nameValue.text = item.itemName
            itemBinding.totalValue.text = stringTotal

            if (position + 1 == itemCount) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }
        }

    }

    private class MutasiDetailDiffUtil : DiffUtil.ItemCallback<MutasiListRequest.Data.Product>() {
        override fun areItemsTheSame(
            oldItem: MutasiListRequest.Data.Product,
            newItem: MutasiListRequest.Data.Product
        ): Boolean {
            return oldItem.itemId == newItem.itemId
        }

        override fun areContentsTheSame(
            oldItem: MutasiListRequest.Data.Product,
            newItem: MutasiListRequest.Data.Product
        ): Boolean {
            return oldItem == newItem
        }

    }

}