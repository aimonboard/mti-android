package com.mti.pos.ui.salesman.mutasi_detail

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.gson.Gson
import com.mti.pos.R
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.databinding.FragmentMutasiDetailBinding
import com.mti.pos.ui.salesman.adapter.MutasiDetailAdapter
import com.mti.pos.util.EnumUtil
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dialog_success.view.*

@AndroidEntryPoint
class MutasiDetailFragment : Fragment() {

    private lateinit var binding: FragmentMutasiDetailBinding
    private lateinit var adapter: MutasiDetailAdapter
    private val viewModel: MutasiDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMutasiDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        adapter = MutasiDetailAdapter()
        binding.recyclerMutasiProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupObserver()
    }

    private fun setupView() {
        binding.apply {
            val mutasiData = Gson().fromJson(
                requireArguments().getString("data"),
                MutasiListRequest.Data::class.java
            )
            viewModel.mutasiId = mutasiData.mutasiId ?: ""
            hasProduct = !mutasiData.product.isNullOrEmpty()
            adapter.submitList(mutasiData.product)

            when (mutasiData.status) {
                EnumUtil.MutasiStatus.Pending.code.toString() -> {
                    statusValue.text = "Pengajuan"
                    statusValue.setBackgroundResource(R.drawable.shape_button_orange)
                }
                EnumUtil.MutasiStatus.Approve.code.toString() -> {
                    statusValue.text = "Diterima"
                    statusValue.setBackgroundResource(R.drawable.shape_button_green)
                }
                EnumUtil.MutasiStatus.Reject.code.toString() -> {
                    statusValue.text = "Ditolak"
                    statusValue.setBackgroundResource(R.drawable.shape_button_red)
                }
            }
            numberValue.text = mutasiData.nomorMutasi ?: "-"
            requestDateValue.text = mutasiData.date ?: "-"
            requestByValue.text = mutasiData.requestBy?.first()?.staffName ?: "-"
            buttonLay.visibility =
                if (mutasiData.status == EnumUtil.MutasiStatus.Pending.code.toString()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

            btnBack.setOnClickListener {
                requireActivity().onBackPressed()
            }
            btnApprove.setOnClickListener {
                viewModel.isApprove = EnumUtil.MutasiStatus.Approve.code.toString()
                setMutasi()
            }
            btnReject.setOnClickListener {
                viewModel.isApprove = EnumUtil.MutasiStatus.Reject.code.toString()
                setMutasi()
            }
        }
    }

    private fun setupObserver() {
        viewModel.getProfile().observe(viewLifecycleOwner, { profile ->
            viewModel.userId = profile.first().userId.toString()
        })
    }

    private fun setMutasi() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.setMutasi().observe(viewLifecycleOwner, {
            when (it.status) {
                ApiResponse.ApiStatus.SUCCESS -> {
                    progressDialog.dismiss()
                    successDialog()
                }
                ApiResponse.ApiStatus.ERROR -> {
                    progressDialog.dismiss()
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun successDialog() {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.dialog_success, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)

        dialogView.message.text = resources.getString(R.string.dialog_success)
        dialogView.btn_ok.setOnClickListener {
            alertDialog.dismiss()
            requireActivity().onBackPressed()
        }

        alertDialog.show()
    }

}