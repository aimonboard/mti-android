package com.mti.pos.ui.salesman.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mti.pos.data.model.DeviceModel
import com.mti.pos.databinding.ItemDeviceBinding

class BluetoothNewAdapter (private val listener: NewDeviceListener) : RecyclerView.Adapter<BluetoothNewAdapter.ViewHolder>() {

    private val items = ArrayList<DeviceModel>()

    fun setItems(items: List<DeviceModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: DeviceModel) {
        if (!this.items.contains(item)) {
            this.items.add(item)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemDeviceBinding = ItemDeviceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position, items[position])

    inner class ViewHolder(private val itemBinding: ItemDeviceBinding, private val listener: NewDeviceListener) : RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int, item: DeviceModel) {
            itemBinding.deviceName.text = item.deviceName
            itemBinding.deviceAddress.text = item.deviceAddress

            if (position == items.size-1) {
                itemBinding.divider.visibility = View.GONE
            } else {
                itemBinding.divider.visibility = View.VISIBLE
            }

            itemBinding.root.setOnClickListener {
                listener.onPairDevice(item)
            }
        }
    }

    interface NewDeviceListener {
        fun onPairDevice(device: DeviceModel)
    }

}