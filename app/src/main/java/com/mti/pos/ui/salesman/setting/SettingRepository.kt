package com.mti.pos.ui.salesman.setting

import android.bluetooth.BluetoothAdapter
import com.mti.pos.data.model.DeviceModel
import javax.inject.Inject

class SettingRepository @Inject constructor(
    private val btAdapter: BluetoothAdapter
) {


    fun getPairDevice(): List<DeviceModel> {
        val pairedData = ArrayList<DeviceModel>()
        btAdapter.bondedDevices.forEach{
            pairedData.add(
                DeviceModel(
                    deviceName = it.name,
                    deviceAddress = it.address,
                )
            )
        }
        return pairedData
    }

    fun pairDevice(deviceModel: DeviceModel) {
        val device = btAdapter.getRemoteDevice(deviceModel.deviceAddress)
        device.createBond()
    }

}