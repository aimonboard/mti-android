package com.mti.pos.ui.salesman.transaction

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.Result
import com.mti.pos.R
import com.mti.pos.custom.CustomViewFinder
import com.mti.pos.data.SharePreferenceData
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.data.model.ProductStockPriceModel
import com.mti.pos.databinding.FragmentTransactionBinding
import com.mti.pos.ui.salesman.adapter.TransCustomerAdapter
import com.mti.pos.ui.salesman.adapter.TransProductAddAdapter
import com.mti.pos.ui.salesman.adapter.TransProductViewAdapter
import com.mti.pos.util.EnumUtil
import dagger.hilt.android.AndroidEntryPoint
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView


@AndroidEntryPoint
class TransactionFragment : Fragment(), TransProductViewAdapter.ProductItemListener,
    ZXingScannerView.ResultHandler {

    private lateinit var binding: FragmentTransactionBinding
    private lateinit var productViewAdapter: TransProductViewAdapter
    private lateinit var mScannerView: ZXingScannerView
    private lateinit var mScanDialog: Dialog
    private val viewModel: TransactionViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTransactionBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        productViewAdapter = TransProductViewAdapter(this)
        binding.recyclerProductView.adapter = productViewAdapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.enableBluetooth(true)
        setupObservers()
        textWatcher()
        buttonListener()
    }

    override fun onProductDelete(product: ProductStockPriceModel) {
        viewModel.removeProduct(product)
    }

    override fun handleResult(rawResult: Result?) {
        if (rawResult != null) {
            mScanDialog.dismiss()
            val customerId = rawResult.toString().toIntOrNull() ?: 0
            viewModel.getCustomerById(customerId).observe(viewLifecycleOwner, Observer { it ->
                if (it.isNotEmpty()) {
                    // customer found
                    Log.e("aim", "scan : customer found, $it")
                    binding.btnAddProduct.visibility = View.VISIBLE
                    viewModel.clearProduct()

                    viewModel.customerId = it.first().customerId
                    viewModel.customerType = it.first().customer_customerTypeId
                    viewModel.customerNameBinding.value = it.first().customerName
                } else {
                    // customer not found
                    Log.e("aim", "scan : customer not found")
                    Toast.makeText(requireContext(), "Pelanggan Tidak Ditemukan", Toast.LENGTH_LONG)
                        .show()
                }
            })
        }
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnAddCustomer.setOnClickListener {
            customerDialog()
        }

        binding.btnAddProduct.setOnClickListener {
            productAddDialog()
        }

        binding.btnScanQr.setOnClickListener {
            scanQRDialog()
        }

        binding.btnCredit.setOnClickListener {
            submitTransaction(
                transType = EnumUtil.TransType.Credit.code
            )
        }

        binding.btnCash.setOnClickListener {
            submitTransaction(
                transType = EnumUtil.TransType.Cash.code
            )
        }
    }

    private fun setupObservers() {
        viewModel.getProfile().observe(viewLifecycleOwner, Observer {
            viewModel.userId = it.first().userId
            viewModel.userName = it.first().userName
            viewModel.warehouseCode = it.first().warehouseCode

            if (it.first().transType == EnumUtil.TransType.Cash.code) {
                binding.btnCredit.isVisible = false
            }
        })

        viewModel.productLive.observe(viewLifecycleOwner, Observer {
            Log.e("aim", "product : $it")
            binding.hasProduct = !it.isNullOrEmpty()
            productViewAdapter.setItems(it)
        })

        viewModel.isButtonEnable.observe(viewLifecycleOwner, Observer {
            binding.btnCredit.isEnabled = it
            binding.btnCash.isEnabled = it
        })
    }

    private fun textWatcher() {
        binding.discountValue.setSeparator(".")
        binding.discountValue.setDecimals(false)
        binding.discountValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                viewModel.discountTextWatcher(binding.discountValue.cleanDoubleValue)
            }
        })

        binding.cashValue.setSeparator(".")
        binding.cashValue.setDecimals(false)
        binding.cashValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                viewModel.paymentTextWatcher(binding.cashValue.cleanDoubleValue)
            }
        })
    }

    private fun scanQRDialog() {
        mScanDialog = Dialog(requireContext(), R.style.DialogAnimation)
        mScanDialog.setContentView(R.layout.dialog_scan_customer)
        mScanDialog.setCancelable(true)
        mScanDialog.show()

        val btnClose = mScanDialog.findViewById<ImageView>(R.id.btn_close)
        val frameLayoutCamera = mScanDialog.findViewById<FrameLayout>(R.id.frame_layout_camera)

        mScannerView = object : ZXingScannerView(requireContext()) {
            override fun createViewFinderView(context: Context?): IViewFinder {
                return CustomViewFinder(requireContext())
            }
        }

        mScannerView.startCamera()
        mScannerView.setAutoFocus(true)
        mScannerView.setResultHandler(this)
        frameLayoutCamera.addView(mScannerView)

        btnClose.setOnClickListener {
            mScanDialog.dismiss()
            mScannerView.stopCamera()
        }
    }

    private fun customerDialog() {
        val dialog = Dialog(requireContext(), R.style.DialogAnimation)
        dialog.setContentView(R.layout.dialog_add_customer)
        dialog.setCancelable(true)
        dialog.show()

        val btnClose = dialog.findViewById<ImageView>(R.id.btn_close)
        val recycler = dialog.findViewById<RecyclerView>(R.id.recycler_customer)
        val warningText = dialog.findViewById<TextView>(R.id.warning_text)
        val customerAdapter = TransCustomerAdapter()
        recycler.adapter = customerAdapter

        customerAdapter.setListener(object : TransCustomerAdapter.CustomerItemListener {
            override fun onCustomerClick(customerData: CustomerEntity) {
                Log.e("aim", "customer data : $customerData")
                binding.btnAddProduct.visibility = View.VISIBLE
                viewModel.clearProduct()

                viewModel.customerId = customerData.customerId
                viewModel.customerType = customerData.customer_customerTypeId
                viewModel.customerNameBinding.value = customerData.customerName
                dialog.dismiss()
            }
        })

        viewModel.getCustomer("").observe(viewLifecycleOwner, Observer {
            customerAdapter.submitList(it)
            if (it.isNotEmpty()) {
                warningText.visibility = View.GONE
            } else {
                warningText.visibility = View.VISIBLE
            }
        })

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun productAddDialog() {
        val dialog = Dialog(requireContext(), R.style.DialogAnimation)
        dialog.setContentView(R.layout.dialog_add_product)
        dialog.setCancelable(true)
        dialog.show()

        val btnClose = dialog.findViewById<TextView>(R.id.btn_close)
        val recycler = dialog.findViewById<RecyclerView>(R.id.recycler_product_add)
        val warningText = dialog.findViewById<TextView>(R.id.warning_text)
        val productAddAdapter = TransProductAddAdapter()
        recycler.adapter = productAddAdapter

        productAddAdapter.setListener(object : TransProductAddAdapter.ProductItemListener {
            override fun onProductAdd(product: ProductStockPriceModel) {
                viewModel.addProduct(product)
            }

            override fun onProductDelete(product: ProductStockPriceModel) {
                viewModel.removeProduct(product)
            }
        })

        viewModel.getProduct("").observe(viewLifecycleOwner, Observer {
            productAddAdapter.setItems(it)
            if (it.isNotEmpty()) {
                warningText.visibility = View.GONE
            } else {
                warningText.visibility = View.VISIBLE
            }
        })

        btnClose.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun submitTransaction(transType: Int) {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Mengirim data")
        progressDialog.setCancelable(false)
        progressDialog.show()

        viewModel.transactionType = if (transType == EnumUtil.TransType.Credit.code) {
            EnumUtil.TransType.Credit.code
        } else {
            EnumUtil.TransType.Cash.code
        }

        generateTransactionCode()

        viewModel.sendTransaction()

        viewModel.getTransactionId().observe(viewLifecycleOwner, Observer {
            // wait for database process
            Handler(Looper.getMainLooper()).postDelayed({
                progressDialog.dismiss()
                viewModel.clearAllData()

                val bundle = Bundle()
                bundle.putInt("transaction_id", it)
                findNavController().navigate(R.id.nav_transaction_detail, bundle)
            }, 3000)

        })
    }

    private fun generateTransactionCode() {
        val currentCode = SharePreferenceData.getInt(
            context = context,
            key = EnumUtil.SharedPref.TransCode.key,
            defValue = 1
        )
        val nextCode = viewModel.incrementTransactionCode(currentCode)
        SharePreferenceData.setInt(
            context = context,
            key = EnumUtil.SharedPref.TransCode.key,
            value = nextCode
        )
    }

}