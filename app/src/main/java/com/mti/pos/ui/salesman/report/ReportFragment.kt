package com.mti.pos.ui.salesman.report

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mti.pos.R
import com.mti.pos.databinding.FragmentReportBinding
import com.mti.pos.ui.salesman.adapter.ReportAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReportFragment : Fragment(), ReportAdapter.ReportItemListener {

    private lateinit var binding: FragmentReportBinding
    private lateinit var adapter: ReportAdapter
    private val viewModel : ReportViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentReportBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        adapter = ReportAdapter(this)
        binding.recyclerProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        generateSpinner()
        setupObserver()
        buttonListener()
    }

    override fun onItemClick(transactionId: Int) {
        val bundle = Bundle()
        bundle.putInt("transaction_id", transactionId)
        findNavController().navigate(R.id.nav_report_detail, bundle)
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if(!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    private fun generateSpinner() {
        val typeData = arrayOf(
            "Hari ini",
            "Minggu ini",
            "Bulan ini",
            "Tahun ini"
        )
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.item_spinner, typeData)
        binding.spinnerFilter.adapter = arrayAdapter

        binding.spinnerFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.selectedSpinner(position)
                setupObserver()
            }
        }
    }

    private fun setupObserver() {
        viewModel.getHeaderTransaction().observe(viewLifecycleOwner, Observer {
            binding.hasProduct = !it.isNullOrEmpty()
            adapter.submitList(it)
        })
    }



}