package com.mti.pos.ui.salesman.mutasi_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.retrofit.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import okhttp3.ResponseBody
import javax.inject.Inject

@HiltViewModel
class MutasiDetailViewModel @Inject constructor(
    private val repository: MutasiDetailRepository
) : ViewModel() {

    var userId = ""
    var mutasiId = ""
    var isApprove = ""

    fun getProfile(): LiveData<List<ProfileEntity>> {
        return repository.getProfile()
    }

    fun setMutasi(): LiveData<ApiResponse<ResponseBody>> {
        return repository.setMutasi(
            userId = userId,
            mutasiId = mutasiId,
            isApprove = isApprove
        )
    }

}