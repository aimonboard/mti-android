package com.mti.pos.ui.salesman.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mti.pos.R
import com.mti.pos.data.database.entity.CustomerEntity
import com.mti.pos.databinding.FragmentCustomerBinding
import com.mti.pos.ui.salesman.adapter.CustomerAdapter
import com.mti.pos.util.KeyboardHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CustomerFragment : Fragment() {

    private lateinit var binding: FragmentCustomerBinding
    private lateinit var adapter: CustomerAdapter
    private val viewModel: CustomerViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCustomerBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        adapter = CustomerAdapter()
        binding.recyclerCustomer.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        buttonListener()
        handlingEditCustomer()
    }

    private fun buttonListener() {
        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        binding.btnMenu.setOnClickListener {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.btnSearch.setOnClickListener {
            if (binding.headerName.visibility == View.VISIBLE) {
                binding.headerName.visibility = View.GONE
                binding.searchLay.visibility = View.VISIBLE
                binding.btnSearch.setImageResource(R.drawable.ic_close)

                KeyboardHelper.openSoftKeyboard(requireContext(), binding.searchValue)
            } else {
                binding.headerName.visibility = View.VISIBLE
                binding.searchLay.visibility = View.GONE
                binding.btnSearch.setImageResource(R.drawable.ic_search)

                binding.searchValue.setText("")
                KeyboardHelper.hideSoftKeyboard(requireContext(), binding.searchValue)
            }
        }

        binding.btnAddCustomer.setOnClickListener {
            findNavController().navigate(
                R.id.nav_customer_add
            )
        }
    }

    private fun handlingEditCustomer() {
        adapter.setListener(object : CustomerAdapter.CustomerItemListener {
            override fun onCustomerEdit(customerData: CustomerEntity) {
                findNavController().navigate(
                    R.id.nav_customer_add,
                    bundleOf(
                        "customer_id" to customerData.customerId,
                        "customer_name" to customerData.customerName,
                        "customer_address" to customerData.customerAddress,
                        "customer_phone" to customerData.customerPhone,
                        "customer_lat" to customerData.customerLat,
                        "customer_lon" to customerData.customerLon
                    )
                )
            }
        })
    }

    private fun setupObservers() {
        viewModel.customerData.observe(viewLifecycleOwner, Observer {
            binding.hasCustomer = !it.isNullOrEmpty()
            adapter.submitList(it)
        })
    }

}