package com.mti.pos.ui.salesman.transaction_detail

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.mti.pos.MainActivity
import com.mti.pos.databinding.FragmentTransactionDetailBinding
import com.mti.pos.ui.salesman.adapter.TransDetailAdapter
import com.mti.pos.util.Formater
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class TransactionDetailFragment : Fragment() {

    private lateinit var binding: FragmentTransactionDetailBinding
    private lateinit var adapter: TransDetailAdapter
    private val viewModel: TransactionDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTransactionDetailBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        adapter = TransDetailAdapter()
        binding.recyclerTransDetail.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        buttonListener()
        onBackpressListener()
    }

    private fun onBackpressListener() {
        // This callback will only be called when MyFragment is at least Started.
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            activity?.finish()
        }
    }

    private fun buttonListener() {
        binding.btnBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.btnPrint.setOnClickListener {
            selectDeviceDialog()
        }
    }

    private fun setupObservers() {
        val uiFormat = SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault())
        val serverFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

        val transactionId = requireArguments().getInt("transaction_id")
        viewModel.getTransactionHeader(transactionId).observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                viewModel.transactionHeader = it.first()

                val totalString = "Rp ${Formater().idrFormat(it.first().transGrandTotal)}"
                val paymentString = "Rp ${Formater().idrFormat(it.first().transPayment)}"
                val cashBackString = "Rp ${Formater().idrFormat(it.first().transCashBack)}"

                viewModel.kodeBinding.value = it.first().transCode ?: "-"
                val dateObj = serverFormat.parse(it.first().transDate)
                viewModel.dateBinding.value = uiFormat.format(dateObj!!)

                viewModel.customerBinding.value = it.first().trans_customerName ?: "-"
                viewModel.totalBinding.value = totalString
                viewModel.paymentBinding.value = paymentString
                viewModel.cashBackBinding.value = cashBackString
            }
        })

        viewModel.getTransactionDetail(transactionId).observe(viewLifecycleOwner, Observer {
            viewModel.transactionDetail = it

            binding.hasProduct = !it.isNullOrEmpty()
            adapter.submitList(it)
        })
    }

    private fun selectDeviceDialog() {
        val data = viewModel.getPairedDevice()
        val deviceName = Array(data.size) { "" }
        data.forEachIndexed { index, it ->
            deviceName[index] = it.deviceName
        }

        val builder = AlertDialog.Builder(requireContext())
        builder.setCancelable(true)
        builder.setTitle("Pilih Printer").setItems(deviceName) { _, which ->
            viewModel.printTransaction()
        }

        builder.create()
        builder.show()
    }

}