package com.mti.pos.ui.salesman.mutasi

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mti.pos.data.database.AppDao
import com.mti.pos.data.database.entity.ProfileEntity
import com.mti.pos.data.remote.MutasiListRequest
import com.mti.pos.data.retrofit.ApiResponse
import com.mti.pos.data.retrofit.ApiService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MutasiRepository @Inject constructor(
    private val remoteDataSource: ApiService,
    private val localDataSource: AppDao
) {

    fun getProfile(): LiveData<List<ProfileEntity>> {
        val profileData = MutableLiveData<List<ProfileEntity>>()
        GlobalScope.launch {
            profileData.postValue(localDataSource.profileGetAll())
        }
        return profileData
    }

    fun getMutasi(userId: String): MutableLiveData<ApiResponse<MutasiListRequest>> {
        val responseData = MutableLiveData<ApiResponse<MutasiListRequest>>()

        val params = HashMap<String, String>()
        params["staff_id"] = userId
        Log.e("aim", "mutasi body : $params")

        remoteDataSource.getMutasi(params).enqueue(object : Callback<MutasiListRequest> {
            override fun onFailure(call: Call<MutasiListRequest>, t: Throwable) {
                responseData.value = ApiResponse.error(
                    "Error ${t.message}",
                    null
                )
            }

            override fun onResponse(
                call: Call<MutasiListRequest>,
                response: Response<MutasiListRequest>
            ) {
                if (response.code() == 200) {
                    responseData.value = ApiResponse.success(
                        response.body()
                    )
                } else {
                    responseData.value = ApiResponse.error(
                        "Error",
                        null
                    )
                }
            }
        })

        return responseData
    }

}